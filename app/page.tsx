import * as React from "react"

import type { Metadata } from "next"

import CategoryPostsSection from "app/home/categoryPosts"
import TopPosts from "app/home/topPosts"
import ReadyToLearnNowBanner from "components/banners/readyToLearnNow"
import StartLearningNowBanner from "components/banners/startLearningNow"
import CategoriesSlider from "components/categoriesSlider"
import FeaturedPostCard from "components/featuredPostCard"
import Footer from "components/footer"
import SearchInput from "components/searchInput"
import { WP_BLOG_SEARCH_API_ENDPOINT } from "constants/endpoints"
import { getCategoriesListData } from "handlers"
import {
  getCategoriesPostsSectionsData,
  getFeaturedPostsData,
} from "handlers/homPage"
import { getHomePageYoastHeadJson } from "handlers/metaData"

export async function generateMetadata(): Promise<Metadata> {
  const meta = await getHomePageYoastHeadJson()
  return meta
}

const Home: React.FC = async () => {
  const categories = await getCategoriesListData()

  const featuredPromise = getFeaturedPostsData()
  const categoriesPostsPromise = getCategoriesPostsSectionsData(categories)

  const response = await Promise.all([featuredPromise, categoriesPostsPromise])

  const { featuredPosts, topPosts } = response[0]
  const categoriesPostsSections = response[1]
  let categoriesMidpointIndex = Math.floor(categoriesPostsSections.length / 2)
  categoriesMidpointIndex =
    categoriesMidpointIndex > 0 ? categoriesMidpointIndex : 0

  return (
    <React.Fragment>
      <main>
        <section className="cont my-5 flex flex-col space-x-0 space-y-4 md:mb-10 md:mt-6 md:flex-row md:items-center md:justify-center md:space-x-60 md:space-y-0">
          <div className="flex-1">
            <span className="page-title text-center">Scholar Den Blogs</span>
          </div>

          <div className="flex-1">
            <SearchInput
              placeholder="Search keyword here..."
              searchURL={WP_BLOG_SEARCH_API_ENDPOINT + "?search="}
            />
          </div>
        </section>

        <section className="cont">
          <CategoriesSlider
            activeCategorySlug="all"
            title="Top categories for you"
            data={categories}
          />
        </section>

        <TopPosts data={topPosts} />

        <section className="cont">
          <FeaturedPostCard post={featuredPosts} />
        </section>

        {categoriesPostsSections.map((category, index) => {
          let banner: React.ReactElement | null = null

          if (categoriesMidpointIndex === index) {
            banner = <ReadyToLearnNowBanner />
          }
          return (
            <React.Fragment key={category.id}>
              {banner}

              <CategoryPostsSection {...category} />
            </React.Fragment>
          )
        })}
      </main>

      <StartLearningNowBanner />
      <Footer />
    </React.Fragment>
  )
}

export default Home
