const WP_POST_ROBOTS_ENDPOINT = process.env.WP_POST_ROBOTS_ENDPOINT ?? ""

/**
 * Fetches data from the MongoDB API.
 * @returns {Promise<any>} A promise that resolves to the fetched data.
 */

export async function GET(): Promise<any> {
  const response = await fetch(WP_POST_ROBOTS_ENDPOINT)
  const text = await response.text()

  return new Response(String(text), {
    status: 200,
    headers: {
      "Content-Type": "text/plain",
      "Cache-control": "stale-while-revalidate, s-maxage=3600",
    },
  })
}
