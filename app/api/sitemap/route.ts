const WP_POST_SITEMAP_ENDPOINT = process.env.WP_POST_SITEMAP_ENDPOINT ?? ""

/**
 * Fetches data from the MongoDB API.
 * @returns {Promise<any>} A promise that resolves to the fetched data.
 */
export async function GET(): Promise<any> {
  const response = await fetch(WP_POST_SITEMAP_ENDPOINT)
  const xmlData = await response.text()

  let newXml = String(xmlData)
  newXml = newXml.trim()
  newXml = newXml.replaceAll(
    `<?xml-stylesheet type="text/xsl" href="//develop.blog.scholarden.com/wp-content/plugins/wordpress-seo/css/main-sitemap.xsl"?>`,
    ""
  )
  newXml = newXml.replaceAll(
    "https://develop.blog.scholarden.com",
    "https://blog.scholarden.com"
  )
  newXml = newXml.replaceAll(
    "https://blog.scholarden.com/wp-content",
    "https://develop.blog.scholarden.com/wp-content"
  )

  return new Response(String(newXml), {
    status: 200,
    headers: {
      "Content-Type": "text/xml",
      "Cache-control": "stale-while-revalidate, s-maxage=3600",
    },
  })
}
