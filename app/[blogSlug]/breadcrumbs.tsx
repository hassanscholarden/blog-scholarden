/* eslint-disable jsx-a11y/anchor-is-valid */
"use client"

import * as React from "react"

import { Breadcrumbs } from "@material-tailwind/react"
import Link from "next/link"
import { useRouter } from "next/navigation"

import { CATEGORIES_PAGE } from "constants/routes"
import type { PostCategory } from "types/postCards"
import classNames from "utils/classNames"

interface Props {
  category: PostCategory | null
  title: string
}

const BackButton: React.FC<Props> = (props) => {
  const { category, title } = props

  const router = useRouter()

  const handleClick: () => void = () => {
    router.back()
  }

  return (
    <div className={classNames("flex text-lg md:items-center")}>
      <button
        name="back button"
        aria-label="back button"
        aria-labelledby="back button"
        onClick={handleClick}
        className="flex cursor-pointer space-x-2 text-primary-600 md:space-x-0 md:text-gray-800"
      >
        <span>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className={classNames("h-6 w-6")}
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M15.75 19.5L8.25 12l7.5-7.5"
            />
          </svg>
        </span>

        <span className="hidden font-semibold md:inline">Back</span>
      </button>

      <Breadcrumbs
        className="blog-breadcrumb px-3"
        separator={
          <div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              viewBox="0 0 16 16"
              fill="none"
            >
              <g clip-path="url(#clip0_12098_12422)">
                <path
                  d="M6 4L10 8L6 12"
                  stroke="#888B94"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </g>
              <defs>
                <clipPath id="clip0_12098_12422">
                  <rect width="16" height="16" fill="white" />
                </clipPath>
              </defs>
            </svg>
          </div>
        }
      >
        <Link href={"/"} className="breadcrumb-link opacity-80">
          Blog
        </Link>

        {category !== null && (
          <Link
            href={`${CATEGORIES_PAGE}/${category.slug}`}
            className="breadcrumb-link opacity-80"
          >
            <span dangerouslySetInnerHTML={{ __html: category.name }} />
          </Link>
        )}

        <span className="breadcrumb-link cursor-auto text-primary-600">
          <span dangerouslySetInnerHTML={{ __html: title }} />
        </span>
      </Breadcrumbs>
    </div>
  )
}

export default BackButton
