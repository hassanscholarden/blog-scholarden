"use client"

import * as React from "react"

import Image from "next/image"

import Icon from "@core/icon"
import ActionsButtonRow from "components/partials/comments/actionsButtonRow"
import PostCommentForm from "components/partials/comments/postCommentForm"
import { getCommentsListHandler } from "handlers/postComments"
import type { Comment } from "types/comments"
import "styles/comments.css"
import type { promiseHandlerType } from "types/common"
import classNames from "utils/classNames"

interface Props {
  postId: number
}

type getCommentsListType = () => Promise<any>

const CommentsSection: React.FC<Props> = ({ postId }) => {
  const initializedFetchingRef = React.useRef(false)

  const [fetchingComments, setFetchingComments] = React.useState(true)
  const [loadingMoreComments, setLoadingMoreComments] = React.useState(false)
  const [comments, setComments] = React.useState<Comment[]>([])
  const [page, setPage] = React.useState(1)
  const [hasMore, setHasMore] = React.useState(false)

  const getCommentsList: getCommentsListType = async () => {
    if (initializedFetchingRef.current) return

    initializedFetchingRef.current = true

    setFetchingComments(true)
    const { list, hasMoreComments } = await getCommentsListHandler(postId, page)
    setComments(list)
    setHasMore(hasMoreComments)
    setFetchingComments(false)
  }
  const handleLoadMore: promiseHandlerType = async () => {
    if (!hasMore) return

    const newPage = page + 1

    setLoadingMoreComments(true)

    const { list, hasMoreComments } = await getCommentsListHandler(
      postId,
      newPage
    )

    setComments((prev) => [...prev, ...list])

    setHasMore(hasMoreComments)

    if (hasMoreComments) setPage(newPage)
    setLoadingMoreComments(false)
  }

  React.useEffect(() => {
    void getCommentsList()

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const renderCommentCard: (
    comment: Comment,
    allowActions?: boolean
  ) => JSX.Element = (comment, allowActions) => {
    return (
      <span className="flex space-x-3 text-[13px] md:text-[17px] ">
        <span className="blog-comment-avatar">
          {comment.authorAvatarUrl != null ? (
            <Image
              alt={comment.authorName}
              src={comment.authorAvatarUrl}
              width={50}
              height={50}
            />
          ) : (
            <Icon iconName="user" className="h-8 w-8" />
          )}
        </span>

        <div className="flex flex-1 flex-col space-y-4">
          <div>
            <span
              className="blog-comment-title"
              dangerouslySetInnerHTML={{
                __html: comment.authorName,
              }}
            />
            <span
              className="blog-comment-text text-gray-700"
              dangerouslySetInnerHTML={{
                __html: comment.content,
              }}
            />
          </div>
          {comment.replies.length > 0 && (
            <ul className="blog-comment-replies-list my-3 space-y-6">
              {comment.replies.map((replyComment) => {
                return (
                  <li key={replyComment.id}>
                    {renderCommentCard(replyComment)}
                  </li>
                )
              })}
            </ul>
          )}

          {allowActions === true && (
            <ActionsButtonRow
              postId={postId}
              commentId={comment.id}
              date={comment.date}
            />
          )}
        </div>
      </span>
    )
  }

  return (
    <div>
      <PostCommentForm postId={postId} />

      {fetchingComments ? (
        <p className="mt-8">Fetching Comments...</p>
      ) : (
        comments.length > 0 && (
          <div className="mt-8">
            <p className="col-span-2 text-[23px]">All Comments</p>

            <ul className="my-3 space-y-6">
              {comments.map((comment) => (
                <li key={comment.id}>{renderCommentCard(comment, true)}</li>
              ))}
            </ul>
          </div>
        )
      )}

      {!fetchingComments && hasMore && (
        <button
          name="Load More Posts"
          onClick={() => {
            void handleLoadMore()
          }}
          className={classNames(
            "my-4 w-full cursor-pointer rounded-md bg-primary-600 px-5 py-2 text-center text-[17px] font-medium text-white"
          )}
        >
          {loadingMoreComments ? "Loading..." : "Load More"}
        </button>
      )}
    </div>
  )
}

export default CommentsSection
