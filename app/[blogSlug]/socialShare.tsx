"use client"

import * as React from "react"
import {
  EmailShareButton,
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from "react-share"

import Image from "next/image"

import { BASE_URL } from "constants/urls"
import emailFilled from "public/icons/social/email-filled.svg"
import facebookFilled from "public/icons/social/facebook-filled.svg"
// import instagramFilled from "public/icons/social/instagram-filled.svg"
import linkedinFilled from "public/icons/social/linkedin-filled.svg"
import twitterFilled from "public/icons/social/twitter-filled.svg"
import whatsappFilled from "public/icons/social/whatsapp-filled.svg"

interface Props {
  blogSlug: string
  title: string
}

const SocialShare: React.FC<Props> = ({ blogSlug, title }) => {
  const iconButtonClasses =
    "relative w-[31px] h-[31px] select-none disable-user-select"

  const postText = `
  Share the knowledge! Check out this insightful blog post: ${BASE_URL}/${blogSlug}

  #Blogging #KnowledgeSharing #ReadMore
  `

  return (
    <div className="my-8 flex items-center justify-center space-x-4">
      <p className="text-[23px] text-gray-750">Share</p>

      <div className="flex space-x-4">
        <FacebookShareButton
          quote={postText}
          url={BASE_URL}
          hashtag="#Blogging #KnowledgeSharing #ReadMore"
        >
          <div className={iconButtonClasses}>
            <Image src={facebookFilled} alt="facebook" fill priority />
          </div>
        </FacebookShareButton>

        <WhatsappShareButton title={postText} url={BASE_URL}>
          <div className={iconButtonClasses}>
            <Image src={whatsappFilled} alt="whatsapp" fill priority />
          </div>
        </WhatsappShareButton>

        <LinkedinShareButton url={BASE_URL}>
          <div className={iconButtonClasses}>
            <Image src={linkedinFilled} alt="linkedin" fill priority />
          </div>
        </LinkedinShareButton>

        <TwitterShareButton url={BASE_URL}>
          <div className={iconButtonClasses}>
            <Image src={twitterFilled} alt="twitter" fill priority />
          </div>
        </TwitterShareButton>

        <EmailShareButton subject="New title" body="email body" url={BASE_URL}>
          <div className={iconButtonClasses}>
            <Image src={emailFilled} alt="email" fill priority />
          </div>
        </EmailShareButton>
      </div>
    </div>
  )
}

export default SocialShare
