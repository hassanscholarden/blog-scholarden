/* eslint-disable tailwindcss/no-custom-classname */
import * as React from "react"

import type { Metadata } from "next"

import Image from "next/image"

import BlogBreadcrumbs from "./breadcrumbs"
import CommentsSection from "./commentsSection"
import RelatedPosts from "./relatedPosts"
import SocialShare from "./socialShare"
import Icon from "@core/icon"
import MTAvatar from "@core/MaterialTailwind/Avatar"
import Footer from "components/footer"
import ScrollToTop from "components/scrollToTop"
import { getAllPostsSlugs } from "handlers"
import { getPostDetails } from "handlers/blogDetailPage"
import { getPostYoastHeadJson } from "handlers/metaData"
import imagePlaceholder from "public/images/placeholder.png"
import { formatReadingTime, formatDate } from "utils/date"

import "../../styles/blog-detail.css"

interface Props {
  params: { blogSlug: string }
}
interface StaticParamsReturnType {
  blogSlug: string
}

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  const slug = params.blogSlug

  const meta = await getPostYoastHeadJson(slug)

  return meta
}

export const revalidate = 900

export async function generateStaticParams(): Promise<
  StaticParamsReturnType[]
> {
  const slugs = await getAllPostsSlugs()

  return slugs.map((blogSlug) => ({
    blogSlug,
  }))
}

const BlogDetails: React.FC<Props> = async ({ params }) => {
  const { blogSlug } = params

  const details = await getPostDetails(blogSlug)

  if (details == null) {
    return (
      <div className="flex h-full w-full items-center justify-center py-20 text-center">
        Blog not found
      </div>
    )
  }

  const {
    featuredMedia,
    author,
    content,
    title,
    date,
    readingTime,
    commentCount,
    categories,
    id,
  } = details

  return (
    <React.Fragment>
      <main className="cont relative mt-5">
        <BlogBreadcrumbs
          category={details.categories[0] ?? null}
          title={title}
        />

        <div className="p-0 sm:px-5 sm:py-4 md:px-10 md:py-8 lg:px-20 lg:py-10">
          <div className="relative my-4 h-[154px] min-h-[154px] w-full overflow-hidden rounded-[23px] object-cover sm:h-[296px] sm:min-h-[296px] md:h-[430px] md:min-h-[430px] lg:h-[518px] lg:min-h-[518px]">
            <Image
              src={featuredMedia?.url ?? imagePlaceholder}
              alt={title}
              fill
            />
          </div>

          <div className="my-8 grid grid-cols-1 gap-4 sm:grid-cols-2">
            {/* Author */}
            <div className="order-2 flex items-center space-x-2 sm:order-first">
              <MTAvatar
                src={author.avatarUrl}
                alt={author.name}
                className="h-[23px] w-[23px] sm:h-[56px] sm:w-[56px]"
              />

              <div className="flex h-full flex-col justify-center text-[12px] sm:justify-between sm:text-[17px]">
                <h4 className="capitalize">{author.name}</h4>
                <h5 className="hidden capitalize text-gray-750 sm:inline-block">
                  Admin | Scholar Den
                </h5>
              </div>
            </div>

            {/* Icons Row */}
            <div className="flex items-center justify-start space-x-2 text-[12px] sm:justify-end sm:text-[15px] md:text-[17px]">
              {/* Calender Icon */}
              <div className="flex items-center space-x-2">
                <Icon
                  iconName="calender"
                  className="h-[15px] w-[15px] md:h-[23px] md:w-[23px]"
                />

                <span
                  className="inline-block md:hidden"
                  dangerouslySetInnerHTML={{
                    __html: formatDate(date, "mmm dd, yyyy"),
                  }}
                />

                <span
                  className="hidden md:inline-block"
                  dangerouslySetInnerHTML={{
                    __html: formatDate(date, "html dd month, yy"),
                  }}
                />
              </div>

              <span className="text-gray-500">|</span>

              <div className="flex items-center space-x-2">
                <Icon
                  iconName="clock"
                  className="h-[15px] w-[15px] md:h-[23px] md:w-[23px]"
                />

                <span
                  className="inline-block capitalize md:hidden"
                  dangerouslySetInnerHTML={{
                    __html: formatReadingTime(readingTime, "mins"),
                  }}
                />

                <span
                  className="hidden capitalize md:inline-block"
                  dangerouslySetInnerHTML={{
                    __html: formatReadingTime(readingTime, "mins read"),
                  }}
                />
              </div>
              <span className="text-gray-500">|</span>
              <div className="flex items-center space-x-2">
                <Icon
                  iconName="comment"
                  className="h-[15px] w-[15px] md:h-[23px] md:w-[23px]"
                />

                <span>{commentCount}</span>
              </div>
            </div>
          </div>

          <div className="relative p-1">
            <div className="blog-content-container">
              <span dangerouslySetInnerHTML={{ __html: content }} />
            </div>

            <div className="sticky bottom-4 flex justify-end">
              <div className="relative">
                <div className="md:-right-22 bottom-4 md:absolute md:min-w-[100px]">
                  <ScrollToTop fixed={false} hideText={false} />
                </div>
              </div>
            </div>
          </div>

          <SocialShare blogSlug={blogSlug} title={title} />

          <CommentsSection postId={id} />
        </div>
      </main>
      <Footer>
        <RelatedPosts categories={categories} blogSlug={blogSlug} />
      </Footer>
    </React.Fragment>
  )
}

export default BlogDetails
