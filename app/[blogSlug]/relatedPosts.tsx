import * as React from "react"

import Link from "next/link"

import Button from "@core/button"
import PostCard from "components/postCard"
import { CATEGORIES_PAGE } from "constants/routes"
import { getPostsOfCategory } from "handlers/categoryAllPostsPage"
import type { PostCategory } from "types/postCards"

interface Props {
  categories: PostCategory[]
  blogSlug: string
}

const RelatedPosts: React.FC<Props> = async (props) => {
  const { categories, blogSlug } = props

  const slug = categories[0]?.slug ?? ""

  const getPostsOfCategoryParams = {
    categoryIds: categories.map((el) => el.id),
    featured: false,
    perPage: 6,
  }

  let posts = await getPostsOfCategory(getPostsOfCategoryParams)
  posts = posts.filter((item) => item.slug !== blogSlug)
  posts = posts.slice(0, 4)

  if (slug.length < 1) return null

  return (
    <div className="mx-4 py-8 md:mx-14 lg:mx-20">
      <div className="flex justify-between pb-8">
        <span className="text-[23px] font-semibold md:text-[32px]">
          Related Posts
        </span>

        <Link href={`${CATEGORIES_PAGE}/${slug}`}>
          <Button
            name="View all posts"
            variant="text"
            className="font-semibold text-primary-600 hover:bg-primary-200"
          >
            View all posts
          </Button>
        </Link>
      </div>

      <div className="hide-more-than-three-sm mt-8 flex flex-col space-y-4 md:space-y-16">
        {posts.map((post) => (
          <PostCard key={post.id} data={post} variant="v5" />
        ))}
      </div>
    </div>
  )
}

export default RelatedPosts
