/* eslint-disable @typescript-eslint/no-unused-vars */
import * as React from "react"

import localFont from "next/font/local"

// import { Inter } from "next/font/google"

import GlobalScripts from "layouts/globalScripts"
import MasterLayout from "layouts/MasterLayout"
import classNames from "utils/classNames"

import "../styles/main.css"

// const inter = Inter({
//   variable: "--inter-font",
//   subsets: ["latin"],
// })

export const metadata = {
  title: "Scholar Den Blog",
  description: "Experience the Official GRE Content",
}

interface Props {
  children: React.ReactNode
}

const RootLayout: React.FC<Props> = ({ children }) => {
  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        {/* Icons */}
        <link rel="icon" href="/favicon.ico" />
        <link rel="apple-touch-icon" href="/sd-icon.png" />
        <link rel="mask-icon" href="/sd-icon.png" color="#5B18CA" />
        <meta name="msapplication-TileColor" content="#5B18CA" />
        <meta name="theme-color" content="#5B18CA" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <meta
          name="google-site-verification"
          content="xWo7RCG5_w10OSY1uSiXibJxoXNm6YO_R5XVFtDN6o0"
        />
      </head>

      <body
        className={classNames(fellix.variable, "bg-zinc-900 overflow-x-hidden")}
      >
        <MasterLayout>{children}</MasterLayout>

        <GlobalScripts />
      </body>
    </html>
  )
}

export default RootLayout

const fellix = localFont({
  src: [
    {
      path: "../public/fonts/Fellix-SemiBoldItalic.ttf",
      weight: "600",
      style: "italic",
    },

    {
      path: "../public/fonts/Fellix-SemiBold.ttf",
      weight: "600",
      style: "normal",
    },

    {
      path: "../public/fonts/Fellix-Light.ttf",
      weight: "300",
      style: "normal",
    },

    {
      path: "../public/fonts/Fellix-Black.ttf",
      weight: "900",
      style: "normal",
    },

    {
      path: "../public/fonts/Fellix-BlackItalic.ttf",
      weight: "900",
      style: "italic",
    },

    {
      path: "../public/fonts/Fellix-ExtraBold.ttf",
      weight: "800",
      style: "normal",
    },

    {
      path: "../public/fonts/Fellix-ThinItalic.ttf",
      weight: "100",
      style: "italic",
    },

    {
      path: "../public/fonts/Fellix-Medium.ttf",
      weight: "500",
      style: "normal",
    },

    {
      path: "../public/fonts/Fellix-LightItalic.ttf",
      weight: "300",
      style: "italic",
    },

    {
      path: "../public/fonts/Fellix-BoldItalic.ttf",
      weight: "bold",
      style: "italic",
    },

    {
      path: "../public/fonts/Fellix-Thin.ttf",
      weight: "100",
      style: "normal",
    },

    {
      path: "../public/fonts/Fellix-Regular.ttf",
      weight: "normal",
      style: "normal",
    },

    {
      path: "../public/fonts/Fellix-Bold.ttf",
      weight: "bold",
      style: "normal",
    },

    {
      path: "../public/fonts/Fellix-RegularItalic.ttf",
      weight: "normal",
      style: "italic",
    },

    {
      path: "../public/fonts/Fellix-ExtraBoldItalic.ttf",
      weight: "800",
      style: "italic",
    },
    {
      path: "../public/fonts/Fellix-MediumItalic.ttf",
      weight: "500",
      style: "italic",
    },
  ],
  variable: "--font-fellix",
})
