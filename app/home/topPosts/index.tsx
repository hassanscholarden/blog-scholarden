import * as React from "react"

import PostCard from "components/postCard"
import type { PostCard as PostCardType } from "types/postCards"

interface Props {
  data: PostCardType[]
}

const TopPosts: React.FC<Props> = (props) => {
  const { data } = props

  return (
    <div className="my-6 space-y-4 bg-primary-100 p-[15px] md:mx-14 md:rounded-[23px] md:p-[23px] lg:mx-20">
      <p className="section-title-with-bottom-border">Top Picks for you</p>

      <div className="hide-more-than-three-sm grid grid-cols-1 gap-8 md:grid-cols-2 md:gap-14">
        {data.map((post) => {
          return <PostCard key={post.id} variant="v1" data={post} />
        })}
      </div>
    </div>
  )
}

export default TopPosts
