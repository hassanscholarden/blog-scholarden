import * as React from "react"

import Link from "next/link"

import PostCard from "components/postCard"
import { CATEGORIES_PAGE } from "constants/routes"
import type { CategoryPostSection } from "types/home"

const CategoryPostsSection: React.FC<CategoryPostSection> = (props) => {
  const { name, posts, slug } = props

  return (
    <div className="my-2 space-y-4 rounded-[22px] p-[15px] md:mx-14 md:my-6 md:space-y-6 md:p-[22px] lg:mx-20">
      <p
        className="section-title-with-bottom-border"
        dangerouslySetInnerHTML={{ __html: name }}
      />

      <div className="hide-more-than-three-sm grid grid-cols-1 gap-8 md:grid-cols-2 md:gap-14">
        {posts.map((post) => (
          <PostCard key={post.id} variant="v2" data={post} />
        ))}
      </div>

      <div className="flex items-center justify-center">
        <Link href={`${CATEGORIES_PAGE}/${slug}`} className="show-more-button">
          Show More Posts
        </Link>
      </div>
    </div>
  )
}

export default CategoryPostsSection
