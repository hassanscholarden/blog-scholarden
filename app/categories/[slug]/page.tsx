import * as React from "react"

import type { Metadata } from "next"

import BackButton from "@core/button/backButton"
import CategoryAllPostsSection from "app/categories/[slug]/allPostsSection"
import CategoriesList from "app/categories/[slug]/categoriesList"
import CategoryTopPostsSection from "app/categories/[slug]/topPostsSection"
import StartLearningNowBanner from "components/banners/startLearningNow"
import Footer from "components/footer"
import SearchInput from "components/searchInput"
import { WP_BLOG_SEARCH_API_ENDPOINT } from "constants/endpoints"
import { getAllCategoriesSlugs, getCategoriesListData } from "handlers"
import { getCategoryYoastHeadJson } from "handlers/metaData"
import { getCategoryBySlug } from "utils/blogs/categoryFormatters"

interface Props {
  params: { slug: string }
}
interface StaticParamsReturnType {
  slug: string
}

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  const { slug } = params
  const meta = await getCategoryYoastHeadJson(slug)
  return meta
}

export const revalidate = 900

export async function generateStaticParams(): Promise<
  StaticParamsReturnType[]
> {
  const slugs = await getAllCategoriesSlugs()

  return slugs.map((slug) => ({
    slug,
  }))
}

const CategoryPosts: React.FC<Props> = async ({ params }) => {
  const { slug } = params

  const categories = await getCategoriesListData()

  const category = getCategoryBySlug(categories, slug)

  if (category?.id === undefined) {
    return (
      <div className="flex h-full w-full items-center justify-center py-20 text-center">
        Category Not Found
      </div>
    )
  }

  const { id, name } = category

  return (
    <React.Fragment>
      <main>
        <section className="cont my-4 flex flex-col justify-between space-x-0 space-y-8 md:flex-row md:justify-center md:space-x-4 md:space-y-0">
          <div className="flex flex-1 items-start">
            <BackButton
              variant="text"
              title="Back"
              buttonClasses="text-primary-500"
            />

            <span className="page-title">
              <span
                dangerouslySetInnerHTML={{ __html: name }}
                className="text-left text-gray-800 md:text-primary-600"
              />
            </span>
          </div>

          <div className="md:w-full md:max-w-md">
            <SearchInput
              placeholder="Search keyword here..."
              searchURL={
                WP_BLOG_SEARCH_API_ENDPOINT + `?categories=${id}&search=`
              }
            />
          </div>
        </section>

        <section className="cont">
          <CategoriesList slug={slug} />
        </section>

        <CategoryTopPostsSection categoryId={id} slug={slug} />

        <CategoryAllPostsSection categoryId={id} />
      </main>

      <StartLearningNowBanner />
      <Footer />
    </React.Fragment>
  )
}

export default CategoryPosts
