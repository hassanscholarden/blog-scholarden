import * as React from "react"

import FeaturedPostCard from "components/featuredPostCard"
import PostCard from "components/postCard"
import { getFeaturedPostsOfCategoryData } from "handlers/categoryAllPostsPage"

interface Props {
  slug: string
  categoryId: number
}

const CategoryTopPostsSection: React.FC<Props> = async ({ categoryId }) => {
  const { featuredPosts, topPosts } = await getFeaturedPostsOfCategoryData(
    categoryId
  )

  return (
    <>
      <section className="cont">
        <FeaturedPostCard post={featuredPosts} />
      </section>

      {topPosts.length > 0 && (
        <div className="my-6 space-y-2 bg-primary-100 p-[15px] md:mx-14 md:space-y-4 md:rounded-[23px] md:p-[23px] lg:mx-20">
          <p className="section-title-with-bottom-border">Most popular posts</p>

          <div className="grid grid-cols-1 justify-center gap-8 py-4 sm:grid-cols-2 md:grid-cols-3 md:gap-14">
            {topPosts.map((post) => (
              <React.Fragment key={post.id}>
                <span className="hidden md:inline-block">
                  <PostCard variant="v3" data={post} />
                </span>

                <span className="inline-block md:hidden">
                  <PostCard variant="v1" data={post} />
                </span>
              </React.Fragment>
            ))}
          </div>
        </div>
      )}
    </>
  )
}

export default CategoryTopPostsSection
