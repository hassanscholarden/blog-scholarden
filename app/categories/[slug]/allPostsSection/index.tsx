import * as React from "react"

import LoadMorePosts from "./loadMorePosts"
import DownloadAppNowBanner from "components/banners/downloadAppNow"
import PostCard from "components/postCard"
import { getAllPostsOfCategories } from "handlers/categoryAllPostsPage"

interface Props {
  categoryId: number
}
const CategoryAllPostsSection: React.FC<Props> = async (props) => {
  const { categoryId } = props

  const { posts, perPage, page, hasMore } = await getAllPostsOfCategories(
    categoryId,
    1,
    8
  )
  let postsMidpointIndex = 0

  postsMidpointIndex = Math.floor(posts.length / 2)
  postsMidpointIndex = postsMidpointIndex > 0 ? postsMidpointIndex : 0

  if (!Array.isArray(posts) || posts.length < 1) {
    return (
      <div className="flex h-full w-full items-center justify-center py-20 text-center">
        No Blog Posts Found
      </div>
    )
  }

  return (
    <div className="my-6 space-y-2 rounded-[23px] p-[23px] md:mx-14 md:space-y-4 lg:mx-20">
      <p className="section-title-with-bottom-border">All Posts</p>

      <div className="grid grid-cols-1 gap-8 py-4 md:gap-16">
        {posts.map((post, index) => {
          let banner: React.ReactElement | null = null

          if (postsMidpointIndex === index) {
            banner = <DownloadAppNowBanner />
          }

          return (
            <React.Fragment key={post.id}>
              {banner}

              <span className="hidden md:inline-block">
                <PostCard variant="v4" data={post} />
              </span>

              <span className="inline-block md:hidden">
                <PostCard variant="v2" data={post} />
              </span>
            </React.Fragment>
          )
        })}

        <LoadMorePosts
          categoryId={categoryId}
          perPage={perPage}
          page={page}
          hasMore={hasMore}
        />
      </div>
    </div>
  )
}

export default CategoryAllPostsSection
