/* eslint-disable @typescript-eslint/no-misused-promises */
"use client"

import * as React from "react"

import PostCard from "components/postCard"
import { getAllPostsOfCategories } from "handlers/categoryAllPostsPage"
import type { PostCard as PostCardType } from "types/postCards"

interface Props {
  categoryId: number
  page: number
  perPage: number
  hasMore: boolean
}

const LoadMorePosts: React.FC<Props> = (props) => {
  const { categoryId, perPage, page, hasMore } = props

  const [posts, setPosts] = React.useState<PostCardType[]>([])
  const [loading, setLoading] = React.useState(false)
  const [meta, setMeta] = React.useState({ perPage, page, hasMore })

  const loadMoreHandler: () => Promise<any> = async () => {
    try {
      setLoading(true)

      const { posts, perPage, page, hasMore } = await getAllPostsOfCategories(
        categoryId,
        meta.page + 1,
        meta.perPage
      )
      setPosts((prev) => [...prev, ...posts])
      setMeta({ perPage, page, hasMore })
    } catch (error) {
    } finally {
      setLoading(false)
    }
  }

  return (
    <React.Fragment>
      {posts.map((post) => (
        <React.Fragment key={post.id}>
          <span className="hidden md:inline-block">
            <PostCard variant="v4" data={post} />
          </span>

          <span className="inline-block md:hidden">
            <PostCard variant="v2" data={post} />
          </span>
        </React.Fragment>
      ))}

      <div className="flex items-center justify-center">
        {meta.hasMore ? (
          <button
            onClick={loadMoreHandler}
            className="show-more-button"
            disabled={loading}
          >
            {loading ? "Loading..." : "Show More Posts"}
          </button>
        ) : null}
      </div>
    </React.Fragment>
  )
}

export default LoadMorePosts
