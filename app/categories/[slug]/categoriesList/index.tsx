import * as React from "react"

import CategoriesSlider from "components/categoriesSlider"
import { getCategoriesListData } from "handlers"

interface Props {
  slug: string
}

const CategoriesList: React.FC<Props> = async ({ slug }) => {
  const categories = await getCategoriesListData()

  return (
    <CategoriesSlider
      activeCategorySlug={slug}
      data={categories}
      title="All Categories"
    />
  )
}

export default CategoriesList
