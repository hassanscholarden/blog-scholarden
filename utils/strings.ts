export function removeProtocol(url: string): string {
  const protocol = /^(https?:\/\/)?/
  return url.replace(protocol, "")
}

export function extractSlug(str: string): string | null {
  const url = removeProtocol(str)
  const match = url.match(/\/(.*?)\//)
  if (match != null) {
    return match[1]
  } else {
    return null
  }
}

export function formatHyphenSeparatedString(str: string): string {
  return str.replaceAll("-", " ")
}

export const capitalizeFirstLetter: (text: string) => string = (text) =>
  text.charAt(0).toUpperCase() + text.slice(1)

export const capitalizedAllWordsInText: (text: string) => string = (text) => {
  const arr = text.split(" ")
  for (let i = 0; i < arr.length; i++) {
    arr[i] = capitalizeFirstLetter(arr[i])
  }
  return arr.join(" ")
}

export function replaceDevelopUrlToBlogUrl<dataType>(data: dataType): dataType {
  const findUrl = "develop.blog.scholarden.com"
  const replaceUrl = "blog.scholarden.com"

  const findUrl1 = "blog.scholarden.com/wp-content"
  const replaceUrl1 = "develop.blog.scholarden.com/wp-content"

  const findUrl2 = "blog.scholarden.com/wp-json"
  const replaceUrl2 = "develop.blog.scholarden.com/wp-json"

  let dataString = JSON.stringify(data)
  dataString = dataString.replace(new RegExp(findUrl, "g"), replaceUrl)
  dataString = dataString.replace(new RegExp(findUrl1, "g"), replaceUrl1)
  dataString = dataString.replace(new RegExp(findUrl2, "g"), replaceUrl2)

  const result: dataType = JSON.parse(dataString)

  return result
}
