/* eslint-disable @typescript-eslint/naming-convention */
import { getUnixTime } from "../date"
import {
  type PostAuthor,
  type PostCategory,
  type PostTags,
  type PostFeaturedMedia,
  type PostCard,
  type PostDetails,
} from "types/postCards"
import {
  type WpPostAuthor,
  type WpPostTerm,
  type WpPostFeaturedmedia,
  type WpPost,
} from "types/wordpress-response/post"

const formatPostAuthor: (author: WpPostAuthor) => PostAuthor = (author) => {
  const { id, description, name, slug, avatar_urls } = author

  return { id, description, name, slug, avatarUrl: avatar_urls[96] }
}

const formatPostTerm: (term: WpPostTerm[]) => {
  categories: PostCategory[]
  tags: PostTags[]
} = (terms) => {
  const categories: PostCategory[] = []
  const tags: PostTags[] = []

  terms.forEach((term) => {
    term.forEach((t) => {
      if (t.taxonomy === "category") {
        categories.push({
          id: t.id,
          name: t.name,
          slug: t.slug,
          taxonomy: t.taxonomy,
        })
      } else if (t.taxonomy === "post_tag") {
        tags.push({
          id: t.id,
          name: t.name,
          slug: t.slug,
          taxonomy: t.taxonomy,
        })
      }
    })
  })

  return { categories, tags }
}

const formatFeaturedMedia: (
  featuredMedia: WpPostFeaturedmedia[]
) => PostFeaturedMedia | null = (featuredMedia) => {
  let result: PostFeaturedMedia | null = null

  if (featuredMedia.length > 0) {
    const { id, date, slug, alt_text, author, source_url } = featuredMedia[0]

    result = {
      id,
      date: getUnixTime(date),
      slug,
      altText: alt_text,
      author,
      url: source_url,
    }
  }

  return result
}

export const formatPostCard: (posts: WpPost[]) => PostCard[] = (posts) => {
  const formattedPosts: PostCard[] = []

  posts.forEach((post) => {
    const {
      id,
      date,
      modified,
      slug,
      title,
      status,
      type,
      excerpt,
      _embedded,
      yoast_head_json,
      yoast_head,
    } = post
    const author = _embedded.author ?? []
    const term = _embedded["wp:term"] ?? []
    const featuredMedia = _embedded["wp:featuredmedia"] ?? []

    const { categories, tags } = formatPostTerm(term)

    const commentCount = yoast_head_json?.schema["@graph"][0]?.commentCount ?? 0
    const readingTime = yoast_head_json?.twitter_misc["Est. reading time"] ?? ""

    const formattedPost: PostCard = {
      id,
      slug,
      status,
      categories,
      tags,
      type,
      date: getUnixTime(date),
      modified: getUnixTime(modified),
      title: title.rendered,
      excerpt: excerpt.rendered,
      author: formatPostAuthor(author[0]),
      featuredMedia: formatFeaturedMedia(featuredMedia),
      commentCount,
      readingTime,
      yoastHead: yoast_head,
    }

    formattedPosts.push(formattedPost)
  })

  return formattedPosts
}

export const formatPostDetails: (posts: WpPost[]) => PostDetails | null = (
  posts
) => {
  const formattedPosts: PostDetails[] = []

  posts.forEach((post) => {
    const {
      id,
      date,
      modified,
      slug,
      title,
      status,
      type,
      excerpt,
      _embedded,
      content,
      yoast_head_json,
      yoast_head,
    } = post
    const author = _embedded.author ?? []
    const term = _embedded["wp:term"] ?? []
    const featuredMedia = _embedded["wp:featuredmedia"] ?? []

    const { categories, tags } = formatPostTerm(term)

    const commentCount = yoast_head_json?.schema["@graph"][0]?.commentCount ?? 0
    const readingTime = yoast_head_json?.twitter_misc["Est. reading time"] ?? ""

    const formattedPost: PostDetails = {
      id,
      slug,
      status,
      categories,
      tags,
      type,
      date: getUnixTime(date),
      modified: getUnixTime(modified),
      title: title.rendered,
      excerpt: excerpt.rendered,
      author: formatPostAuthor(author[0]),
      featuredMedia: formatFeaturedMedia(featuredMedia),
      content: content.rendered,
      commentCount,
      readingTime,
      yoastHead: yoast_head,
      yoastHeadJson: yoast_head_json,
    }

    formattedPosts.push(formattedPost)
  })

  return formattedPosts[0] ?? null
}
