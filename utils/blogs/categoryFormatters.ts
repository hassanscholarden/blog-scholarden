import type { Category } from "types/category"
import type { WpCategory } from "types/wordpress-response/categories"
import { sortArrayOfObject } from "utils"

type formatCategoriesListType = (wpCategories: WpCategory[]) => Category[]
type formateCategoryType = (wpCategory: WpCategory) => Category
type getCategoryBySlugType = (
  categories: Category[],
  slug: string
) => Category | null

export const formateCategory: formateCategoryType = (wpCategory) => {
  const { id, count, description, name, slug, taxonomy, parent } = wpCategory
  const category: Category = {
    id,
    count,
    description,
    name,
    slug,
    taxonomy,
    parent,
  }

  return category
}
export const formatCategoriesList: formatCategoriesListType = (wpCategory) => {
  const categories: Record<string, Category> = wpCategory.reduce(
    (acc, category) => {
      const { id, count, description, name, slug, taxonomy, parent } = category
      const data = {
        id,
        count,
        description,
        name,
        slug,
        taxonomy,
        parent,
      }

      if (count > 0) {
        return { ...acc, [id]: data }
      }
      return acc
    },
    {}
  )

  const categoriesWithSubcategories: Record<string, Category> = Object.keys(
    categories
  ).reduce((acc, key) => {
    const category = categories[key]
    const { id, parent } = category

    if (parent !== 0) {
      let parentCategory = acc[parent]

      if (parentCategory === undefined) {
        parentCategory = categories[parent]
      }
      let subcategories = parentCategory.subcategories ?? []

      subcategories = [...subcategories, category]
      subcategories = sortArrayOfObject(subcategories, "descending", "id")

      return { ...acc, [parent]: { ...parentCategory, subcategories } }
    }

    return { ...acc, [id]: category }
  }, {})

  const list = Object.values(categoriesWithSubcategories)

  return list
}

export const getCategoryBySlug: getCategoryBySlugType = (categories, slug) => {
  for (const category of categories) {
    if (category.slug === slug) {
      return category
    }

    const subcategories = category.subcategories ?? []
    const subcategory = subcategories.find((el) => el.slug === slug)

    if (subcategory != null) {
      return subcategory
    }
  }

  return null
}

/**
 * Flattens the given array of categories, removing subcategories and returning a new array.
 *
 * @param categories - The array of categories to flatten.
 * @returns The flattened array of categories.
 */
export function flattenCategories(
  categories: Category[],
  validateCount?: boolean
): Category[] {
  return categories.reduce<Category[]>((result, categoryWithSubcategories) => {
    const categoryWithoutSubcategories: Category = {
      ...categoryWithSubcategories,
    }
    const { count, subcategories } = categoryWithSubcategories
    delete categoryWithoutSubcategories.subcategories

    if (validateCount === true) {
      if (count > 0) {
        result.push(categoryWithoutSubcategories)
      }
    } else result.push(categoryWithoutSubcategories)

    if (subcategories != null && subcategories.length > 0) {
      const flattenSubcategories = flattenCategories(subcategories, true)
      result.push(...flattenSubcategories)
    }

    return result
  }, [])
}
