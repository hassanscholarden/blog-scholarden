/* eslint-disable @typescript-eslint/naming-convention */

import type { Comment } from "types/comments"
import type { WpComment } from "types/wordpress-response/comments"

type formatCommentType = (wpComment: WpComment) => Comment
export const formatComment: formatCommentType = ({
  id,
  post,
  parent,
  author,
  author_name,
  author_url,
  date,
  content,
  author_avatar_urls,
}) => ({
  id,
  post,
  parent,
  author,
  authorName: author_name,
  authorUrl: author_url,
  date: new Date(date).getTime(),
  content: content.rendered ?? "",
  authorAvatarUrl: author_avatar_urls["96"] ?? null,
  replies: [],
})

type formatCommentsListType = (wpComments: WpComment[]) => Comment[]
// export const formatCommentsList: formatCommentsListType = (wpComments) => {
//   const list = wpComments.map((wpComment) => formatComment(wpComment))

//   return list
// }

export const formatCommentsList: formatCommentsListType = (comments) => {
  const commentsMap = new Map<number, Comment>()
  const result: Comment[] = []

  // Create a map of comment IDs to their corresponding comment objects
  comments.forEach((wpComment) => {
    const comment = formatComment(wpComment)
    const { id } = comment
    commentsMap.set(id, comment)
  })

  // Organize replies under their parent comments
  comments.forEach((wpComment) => {
    const comment = formatComment(wpComment)
    const { id, parent } = comment
    if (parent === 0) {
      const commentData = commentsMap.get(id)

      if (commentData != null) result.push(commentData)
    } else {
      const parentComment = commentsMap.get(parent)

      if (parentComment != null) {
        const commentData = commentsMap.get(id)

        if (commentData != null) parentComment.replies.push(commentData)
      }
    }
  })

  return result
}
