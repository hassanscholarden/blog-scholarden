export const nthFormatter: (d: number) => "th" | "st" | "nd" | "rd" = (d) => {
  if (d > 3 && d < 21) return "th"
  switch (d % 10) {
    case 1:
      return "st"
    case 2:
      return "nd"
    case 3:
      return "rd"
    default:
      return "th"
  }
}

export const sortArrayOfObject = <T>(
  arrayOfObject: T[],
  sortType: "acceding" | "descending",
  sortByKey: string
): T[] => {
  const sortedArrayOfObject: T[] = [...arrayOfObject]

  sortedArrayOfObject.sort((a, b) => {
    if (sortType === "acceding") {
      return a[sortByKey] - b[sortByKey]
    } else {
      return b[sortByKey] - a[sortByKey]
    }
  })

  return sortedArrayOfObject
}
