import { nthFormatter } from "utils"

const monthsList = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
]

export const getUnixTime: (d: string | number) => number = (d) => {
  const date = new Date(d)

  return date.getTime()
}

type dateFormattingType = "mmm dd, yyyy" | "html dd month, yy" | "mmm, yy"
export const formatDate: (
  d: string | number,
  type: dateFormattingType
) => string = (d, type) => {
  const fullDate = new Date(d)
  const date = fullDate.getDate()
  const month = monthsList[fullDate.getMonth()]

  const fullYear = fullDate.getFullYear()
  const year = fullYear % 100

  if (type === "mmm dd, yyyy") {
    return `${month.slice(0, 3)} ${date}, ${fullYear}`
  } else if (type === "html dd month, yy") {
    return `${date}<sup>${nthFormatter(date)}</sup> ${month}, ${year}`
  } else if (type === "mmm, yy") {
    return `${month.slice(0, 3)}, ${year}`
  }

  return `${date}/${month}/${year}`
}

export function formatReadingTime(
  readingTime: string,
  format: "mins read" | "mins"
): string {
  const minutes = parseInt(readingTime, 10)
  const formattedMinutes = minutes.toString().padStart(2, "0")

  if (format === "mins read") {
    return `${formattedMinutes} mins read`
  } else if (format === "mins") {
    return `${formattedMinutes} mins`
  } else {
    return readingTime
  }
}

export const getTimeDifference: (timestamp: number | string) => string = (
  timestamp: number | string
) => {
  if (timestamp == null) return ""

  const currentDate = new Date()
  const pastDate = new Date(timestamp)

  const timeDiff = currentDate.getTime() - pastDate.getTime()
  const seconds = Math.floor(timeDiff / 1000)

  if (seconds < 60) {
    return `${seconds} second${seconds === 1 ? "" : "s"}`
  }

  const minutes = Math.floor(seconds / 60)
  if (minutes < 60) {
    return `${minutes} minute${minutes === 1 ? "" : "s"}`
  }

  const hours = Math.floor(minutes / 60)
  if (hours < 24) {
    return `${hours} hour${hours === 1 ? "" : "s"}`
  }

  const days = Math.floor(hours / 24)
  if (days < 30) {
    return `${days} day${days === 1 ? "" : "s"}`
  }

  const months = Math.floor(days / 30)
  return `${months} month${months === 1 ? "" : "s"}`
}
