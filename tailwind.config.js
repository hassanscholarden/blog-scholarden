/** @type {import('tailwindcss').Config} */
const withMT = require("@material-tailwind/react/utils/withMT")

const colors = {
  primary: {
    100: "#F8F1FF",
    200: "#EAD9FE",
    300: "#DEC1FF",
    400: "#9C6CEA",
    500: "#792BF8",
    600: "#5B18CA",
  },
  secondary: {
    100: "#FFE8DF",
    200: "#FDCF70",
    300: "#FFBFA8",
    400: "#FF9A76",
    500: "#F69321",
  },
  tertiary: "#0DA8BA",
  white: "#ffffff",
  gray: {
    100: "#fff",
    200: "#f5f5f5",
    300: "#F7F8FA",
    400: "#E9EAF0",
    500: "#D8DAE3",
    600: "#B8BAC3",
    700: "#888B94",
    750: "#4B5166",
    800: "#383C49",
    900: "#14192B",
  },
  success: "#7FBA7A",
  error: "#FC5A5A",
  backgroundColor: "#FFF",
  backgroundColorOne: "#F8F8F8",
  TableBorderColor: "#EFF2F7",
  transparent: "transparent",
}

const fontFamily = {
  sans: ["var(--font-fellix)"],
}

const screens = {
  xs: "0px",
  sm: "600px",
  md: "900px",
  lg: "1200px",
  xl: "1536px",
  iPadPro: "1025px",
}

module.exports = withMT({
  content: [
    "./@core/**/*.{js,ts,jsx,tsx}",
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./layouts/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    colors,
    fontFamily,
    extend: {
      colors,
      keyframes: {
        slideDownAndFade: {
          from: { opacity: 0, transform: "translateY(-10px)" },
          to: { opacity: 1, transform: "translateY(0)" },
        },
        slideLeftAndFade: {
          from: { opacity: 0, transform: "translateX(-10px)" },
          to: { opacity: 1, transform: "translateX(0)" },
        },
        slideUpAndFade: {
          from: { opacity: 0, transform: "translateY(10px)" },
          to: { opacity: 1, transform: "translateY(0)" },
        },
        slideRightAndFade: {
          from: { opacity: 0, transform: "translateX(10px)" },
          to: { opacity: 1, transform: "translateX(0)" },
        },
        slideDown: {
          from: { transform: "translateY(-10px)" },
          to: { transform: "translateY(0)" },
        },
        slideUp: {
          from: { transform: "translateY(10px)" },
          to: { transform: "translateY(0)" },
        },
        slideLeft: {
          from: { transform: "translateX(-10px)" },
          to: { transform: "translateX(0)" },
        },
        slideRight: {
          from: { transform: "translateX(10px)" },
          to: { transform: "translateX(0)" },
        },
        swing: {
          from: { transform: "rotate(3deg)" },
          to: { transform: "rotate(-3deg)" },
        },
        scale: {
          from: { opacity: 0, transform: "scale(0)" },
          to: { opacity: 1, transform: "scale(1)" },
        },
        rise: {
          from: { transform: "translate(-50%, -40%) scale(0.95)" },
          to: { transform: "translate(-50%, -50%) scale(1)" },
        },
      },
      animation: {
        slideDownAndFade:
          "slideDownAndFade 400ms cubic-bezier(0.16, 1, 0.3, 1)",
        slideLeftAndFade:
          "slideLeftAndFade 400ms cubic-bezier(0.16, 1, 0.3, 1)",
        slideUpAndFade: "slideUpAndFade 400ms cubic-bezier(0.16, 1, 0.3, 1)",
        slideRightAndFade:
          "slideRightAndFade 400ms cubic-bezier(0.16, 1, 0.3, 1)",
        slideDown: "slideDown 400ms cubic-bezier(0.16, 1, 0.3, 1)",
        slideUp: "slideUp 400ms cubic-bezier(0.16, 1, 0.3, 1)",
        slideLeft: "slideLeft 400ms cubic-bezier(0.16, 1, 0.3, 1)",
        slideRight: "slideRight 400ms cubic-bezier(0.16, 1, 0.3, 1)",
        swing: "swing 1s infinite ease-in-out alternate",
        scale: "scale 150ms cubic-bezier(0.4, 0, 0.2, 1)",
        rise: "rise 150ms cubic-bezier(0.4, 0, 0.2, 1)",
      },
      fontFamily,
      typography: {
        h1: {
          fontSize: "80px",
          letterSpacing: "-2px",
          fontWeight: 700,
        },
        h2: {
          fontSize: "64px",
          letterSpacing: "-2px",
          fontWeight: 700,
        },
        h3: {
          fontSize: "40px",
          letterSpacing: "-2px",
          fontWeight: 700,
        },
        smallH3: {
          fontSize: "32px",
          letterSpacing: "-2px",
          fontWeight: 700,
        },
        h4: {
          fontSize: "23px",
          fontWeight: 700,
        },
        h5: {
          fontSize: "20px",
          fontWeight: 600,
        },
        bodyLarge: {
          fontWeight: "normal",
          fontSize: "23px",
          display: "block",
          "@media (max-width:600px)": {
            fontSize: "20px",
          },
        },
        largeBody: {
          fontWeight: "normal",
          fontSize: "17px",
          display: "block",
        },
        body: {
          fontWeight: "normal",
          fontSize: "15px",
          lineHeight: "160%",
          display: "block",
        },
        semiBoldBody: {
          fontWeight: 500,
          fontSize: "15px",
          lineHeight: "160%",
          display: "block",
        },
        mediumBoldBody: {
          fontWeight: 600,
          fontSize: "15px",
          lineHeight: "160%",
          display: "block",
        },
        boldBody: {
          fontWeight: 700,
          fontSize: "15px",
          display: "block",
        },
        smallBody: {
          fontWeight: "normal",
          fontSize: "13px",
          display: "block",
        },
        smallLiteBody: {
          fontWeight: 300,
          fontSize: "13px",

          display: "block",
        },
        preTitle: {
          fontWeight: 700,
          fontSize: "15px",
          display: "block",
        },
        button: {
          textTransform: "none",
        },
        smallPreTitle: {
          fontWeight: 700,
          fontSize: "13px",
          display: "block",
        },
        extraSmallTitle: {
          fontWeight: 700,
          fontSize: "12px",
          display: "block",
        },
        extraSmallBody: {
          fontWeight: 700,
          fontSize: "10px",
          display: "block",
        },
      },
      screens,
    },
  },
  plugins: [],
})
