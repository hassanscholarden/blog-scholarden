"use client"

import * as React from "react"

import { useRouter } from "next/navigation"

import type { buttonVariant } from "./type"
import classNames from "utils/classNames"

import Button from "."

interface Props {
  variant: buttonVariant
  buttonClasses?: string
  svgIconClasses?: string
  titleClasses?: string
  title?: string
  smallScreenTitle?: string
}

const BackButton: React.FC<Props> = (props) => {
  const {
    variant = "text",
    title = "",
    buttonClasses = "",
    svgIconClasses = "",
    titleClasses = "",
    smallScreenTitle = "",
  } = props

  const router = useRouter()

  const handleClick: () => void = () => {
    router.back()
  }

  return (
    <Button
      name="back button"
      variant={variant}
      onClick={handleClick}
      className={classNames("flex items-center text-lg", buttonClasses)}
    >
      <div>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="1.5"
          stroke="currentColor"
          className={classNames("h-6 w-6", svgIconClasses)}
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M15.75 19.5L8.25 12l7.5-7.5"
          />
        </svg>
      </div>
      {smallScreenTitle.length > 0 && (
        <span className={classNames("inline-block md:hidden", titleClasses)}>
          {smallScreenTitle}
        </span>
      )}

      <span className={classNames("hidden md:inline-block", titleClasses)}>
        {title}
      </span>
    </Button>
  )
}

export default BackButton
