import * as React from "react"

import { type buttonVariantClassesHandlerType, type ButtonProps } from "./type"
import classNames from "utils/classNames"

const Button: React.FC<ButtonProps> = (props) => {
  const { children, variant, disabled, className = "", ...buttonProps } = props

  const buttonVariantClassesHandler: buttonVariantClassesHandlerType = (
    variant
  ) => {
    const globalClass = `text-center transition-colors duration-300 md:mx-2 md:my-0 px-4 py-2 cursor-pointer`

    let variantClasses = ""

    switch (variant) {
      case "text":
        variantClasses =
          "hover:bg-gray-200 text-gray-900 hover:text-primary-600 rounded-lg"
        break
      case "outlined":
        variantClasses =
          "rounded-full border border-solid border-primary-400 py-1 text-center font-semibold text-primary-400 hover:bg-primary-100"
        break

      default:
        variantClasses = ""
    }

    return classNames(className, variantClasses, globalClass)
  }

  return (
    <button {...buttonProps} className={buttonVariantClassesHandler(variant)}>
      {children}
    </button>
  )
}

export default Button
