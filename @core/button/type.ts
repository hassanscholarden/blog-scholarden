import type * as React from "react"

type HTMLButtonProps = React.DetailedHTMLProps<
  React.ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement
>
export type buttonVariant = "text" | "outlined"

export interface ButtonProps extends HTMLButtonProps {
  children: React.ReactNode
  variant: buttonVariant
}

export type buttonVariantClassesHandlerType = (variant: buttonVariant) => string
