import * as React from "react"

import Image from "next/image"

import IconNameList from "./iconsList"
import type { IconVariants } from "./type"
import classNames from "utils/classNames"

interface Props {
  iconName: IconVariants
  className: string
  priority?: boolean
}

const Icon: React.FC<Props> = (props) => {
  const { iconName, className = "", priority = true } = props

  const icon = IconNameList[iconName]

  return (
    <div className={classNames("relative", className)}>
      <Image src={icon} alt={`${iconName}-icon`} fill priority={priority} />
    </div>
  )
}

export default Icon
