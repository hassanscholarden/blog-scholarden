export type IconVariants =
  | "search"
  | "chevron-up"
  | "chevron-down"
  | "chevron-right"
  | "chevron-left"
  | "comment"
  | "clock"
  | "calender"
  | "cancel"
  | "user"

export type IconNameListType = {
  [key in IconVariants]: any
}
