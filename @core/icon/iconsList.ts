import type { IconNameListType } from "./type"

const IconNameList: IconNameListType = {
  search: require("public/icons/search.svg"),
  "chevron-up": require("public/icons/chevron-up.svg"),
  "chevron-down": require("public/icons/chevron-down.svg"),
  "chevron-right": require("public/icons/chevron-right.svg"),
  "chevron-left": require("public/icons/chevron-left.svg"),
  comment: require("public/icons/comment.svg"),
  clock: require("public/icons/clock.svg"),
  calender: require("public/icons/calender.svg"),
  cancel: require("public/icons/cancel.svg"),
  user: require("public/icons/user.svg"),
}
export default IconNameList
