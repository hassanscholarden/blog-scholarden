"use client"

import { Avatar as MTAvatar, type AvatarProps } from "@material-tailwind/react"

export default MTAvatar

export type MTAvatarProps = AvatarProps
