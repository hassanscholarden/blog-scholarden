"use client"

import * as React from "react"

import { Collapse } from "@material-tailwind/react"

import classNames from "utils/classNames"

interface Props {
  open: boolean
  message: string
  type: "success" | "error" | "warn"
  alertClassName?: string
  onClose: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
}

const CollapseAlert: React.FC<Props> = (props) => {
  const { open, type, message, onClose, alertClassName } = props

  const backgroundColor =
    type === "success"
      ? "bg-success"
      : type === "error"
      ? "bg-error"
      : "bg-secondary-200"

  return (
    <Collapse open={open}>
      <div
        className={classNames(
          "flex items-center space-x-2",
          alertClassName ??
            "rounded-md p-2 text-[13px] font-semibold text-white md:p-3 md:text-[15px]",
          backgroundColor
        )}
      >
        <div className="flex-1" dangerouslySetInnerHTML={{ __html: message }} />
        <button onClick={onClose} name="close alert">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="h-[18px] w-[18px]"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M6 18L18 6M6 6l12 12"
            />
          </svg>
        </button>
      </div>
    </Collapse>
  )
}

export default CollapseAlert
