import {
  DASHBOARD_URL,
  MENTORING_PAGE_URL,
  VOCABULARY_PAGE_URL,
  PRICING_PAGE_URL,
  FACEBOOK_GROUP,
  FACEBOOK_STUDY_GROUP,
  INSTAGRAM_PAGE,
  LINKEDIN_COMPANY_PAGE,
  YOUTUBE_CHANNEL,
} from "./urls"
import blogIcon from "public/icons/social/blog-outline.svg"
import facebookIcon from "public/icons/social/facebook-outline.svg"
import facebookGroupIcon from "public/icons/social/group-outline.svg"
import instagramIcon from "public/icons/social/instagram-outline.svg"
import linkedinIcon from "public/icons/social/linkedIn-outline.svg"
import youtubeIcon from "public/icons/social/youtube-outline.svg"

export const navLinks = [
  {
    title: "Dashboard",
    url: DASHBOARD_URL,
    type: "external",
  },
  {
    title: "Mentoring",
    url: MENTORING_PAGE_URL,
    type: "external",
  },
  {
    title: "Pricing",
    url: PRICING_PAGE_URL,
    type: "external",
  },
]

export const footerAccountLinks = [
  {
    title: "GRE Mentoring",
    url: MENTORING_PAGE_URL,
    type: "external",
  },
  {
    title: "Vocabulary",
    url: VOCABULARY_PAGE_URL,
    type: "external",
  },
  {
    title: "Blogs",
    url: "/",
    type: "internal",
  },
  {
    title: "Pricing",
    url: PRICING_PAGE_URL,
    type: "external",
  },
]

export const socialLinksData = [
  {
    id: 1,
    icon: facebookIcon,
    alt: "Facebook",
    link: FACEBOOK_GROUP,
    linkType: "external",
  },
  {
    id: 2,
    icon: facebookGroupIcon,
    alt: "Facebook Study Group",
    link: FACEBOOK_STUDY_GROUP,
    linkType: "external",
  },
  {
    id: 3,
    icon: youtubeIcon,
    alt: "Youtube Channel",
    link: YOUTUBE_CHANNEL,
    linkType: "external",
  },
  {
    id: 4,
    icon: instagramIcon,
    alt: "Instagram",
    link: INSTAGRAM_PAGE,
    linkType: "external",
  },
  {
    id: 5,
    icon: linkedinIcon,
    alt: "linked In",
    link: LINKEDIN_COMPANY_PAGE,
    linkType: "external",
  },
  {
    id: 6,
    icon: blogIcon,
    alt: "Blogs",
    link: "/",
    linkType: "internal",
  },
]
