export const WP_API_BASE_URL = process.env.NEXT_PUBLIC_API_BASE_URL ?? ""

// ** WORDPRESS REST API ROUTES **
export const WP_BLOG_PAGES_API_ENDPOINT = WP_API_BASE_URL + "/pages"
export const WP_BLOG_POSTS_API_ENDPOINT = WP_API_BASE_URL + "/posts"
export const WP_BLOG_CATEGORIES_API_ENDPOINT = WP_API_BASE_URL + "/categories"
export const WP_BLOG_SEARCH_API_ENDPOINT = WP_API_BASE_URL + "/search"
export const WP_BLOG_COMMENTS_API_ENDPOINT = WP_API_BASE_URL + "/comments"
