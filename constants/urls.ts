export const SCHOLARDEN_URL = "https://scholarden.com"
export const BASE_URL =
  process.env.NEXT_PUBLIC_BASE_URL ?? "https://blog.scholarden.com"

export const DASHBOARD_URL = SCHOLARDEN_URL + "/dashboard"
export const MENTORING_PAGE_URL = SCHOLARDEN_URL + "/mentoring"
export const PRICING_PAGE_URL = SCHOLARDEN_URL + "/pricing"
export const VOCABULARY_PAGE_URL = SCHOLARDEN_URL + "/vocabulary"

export const TERMS_AND_CONDITION_PAGE_URL =
  SCHOLARDEN_URL + "/terms-and-conditions"

export const FACEBOOK_REVIEWS = "https://www.facebook.com/ScholarDen/reviews/"
export const FACEBOOK_STUDY_GROUP =
  "https://www.facebook.com/groups/ScholarDen/"
export const FACEBOOK_GROUP = "https://www.facebook.com/ScholarDen/"
export const FACEBOOK_MESSENGER = "http://m.me/ScholarDen" // "https://www.messenger.com/t/302639616611552";
export const WHATSAPP_DIRECT_MESSAGE_URL = "https://wa.me/19377551251"
export const YOUTUBE_CHANNEL = "https://www.youtube.com/c/scholarden/"
export const LINKEDIN_COMPANY_PAGE =
  "https://www.linkedin.com/company/scholar-den/"
export const INSTAGRAM_PAGE = "https://www.instagram.com/scholar.den/"
export const GRE_VOCABULARY_APP_GOOGLE_PLAY =
  "https://play.google.com/store/apps/details?id=com.scholardenvocabulary"
export const GRE_VOCABULARY_APP_STORE =
  "https://apps.apple.com/pk/app/scholar-den-gre-vocabulary/id6444090677"
