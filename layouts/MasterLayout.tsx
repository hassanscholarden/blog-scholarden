import * as React from "react"

import ThemeProviders from "./providers"
import Header from "components/header"

interface Props {
  children: React.ReactNode
}

const MasterLayout: React.FC<Props> = ({ children }) => {
  return (
    <ThemeProviders>
      <Header />

      <div>{children}</div>
    </ThemeProviders>
  )
}

export default MasterLayout
