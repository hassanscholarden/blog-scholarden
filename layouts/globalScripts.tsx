"use client"

import { useEffect, useRef } from "react"
import type { FC } from "react"

import { type FunctionType } from "types/global"

const GA_MEASUREMENT_ID = process.env.NEXT_PUBLIC_MEASUREMENT_ID ?? ""

const GlobalScripts: FC = () => {
  const initialized = useRef(false)

  const addGTMScript: FunctionType = () => {
    // Add Google Tag Manager scripts
    const script1 = document.createElement("script")
    script1.src = `https://www.googletagmanager.com/gtag/js?id=${GA_MEASUREMENT_ID}`

    script1.async = true
    script1.onload = () => {
      // This function runs when the first script is loaded
      const script2 = document.createElement("script")
      script2.id = "google-analytics"
      script2.innerHTML = `
          window.dataLayer = window.dataLayer || [];
          function gtag(){window.dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', '${GA_MEASUREMENT_ID}');
        `

      // Append the second script to the document
      document.head.appendChild(script2)

      // Mark the scripts as added
      initialized.current = true
    }

    // Append the first script to the document
    document.head.appendChild(script1)
  }

  useEffect(() => {
    if (!initialized.current) {
      addGTMScript()
    }
  }, [])

  return <div className="hidden" />
}

export default GlobalScripts
