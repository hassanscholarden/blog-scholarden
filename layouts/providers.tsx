"use client"
import * as React from "react"

import { ThemeProvider } from "@material-tailwind/react"

interface Props {
  children: React.ReactNode
}

const ThemeProviders: React.FC<Props> = ({ children }) => {
  return <ThemeProvider>{children}</ThemeProvider>
}

export default ThemeProviders
