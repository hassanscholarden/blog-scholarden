/** @type {import('next').NextConfig} */

const nextConfig = {
  swcMinify: true,
  trailingSlash: true,
  reactStrictMode: true,
  images: {
    domains: [
      "blog.scholarden.com",
      "develop.blog.scholarden.com",
      "secure.gravatar.com",
    ],
  },
  async rewrites() {
    return [
      {
        source: "/sitemap.xml",
        destination: "/api/sitemap",
      },
      {
        source: "/robots.txt",
        destination: "/api/robots",
      },
    ]
  },
  async redirects() {
    return [
      {
        source: "/author/admin",
        destination: "/ ",
        permanent: true,
      },
      {
        source: "/flashcard",
        destination: "https://scholarden.com/vocabulary",
        permanent: true,
      },
      {
        source: "/flashcard/:slug",
        destination: "https://scholarden.com/vocabulary",
        permanent: true,
      },
      {
        source: "/flashcard/:slug/feed",
        destination: "https://scholarden.com/vocabulary",
        permanent: true,
      },
      {
        source: "/admission-guides/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/admission-guides/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/admissions-scholarships/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/admissions-scholarships/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/categories/scholarships",
        destination: "/categories/admissions-scholarships-guides/",
        permanent: true,
      },
      {
        source: "/admissions-scholarships-guides/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/admissions-scholarships-guides/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/admissions-scholarships-success-story/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/admissions-scholarships-success-story/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },

      {
        source: "/category/blog_categories/",
        destination: "/ ",
        permanent: true,
      },
      {
        source: "/blog_categories/gre-319-fareed-k-khan",
        destination: "/gre-scores-for-electrical-engineering-ms-programs",
        permanent: true,
      },
      {
        source: "/blog_categories/frequently-asked-questions-about-the-gre",
        destination: "/gre-faqs",
        permanent: true,
      },
      {
        source: "/blog_categories/gre-blog/blog_posts/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/blog_categories/gre-blog/blog_posts/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/blog_categories/gre-general-test-keeps-your-dream-on-track",
        destination: "/ ",
        permanent: true,
      },
      {
        source: "/blog_categories/how-i-scored-315",
        destination: "/how-i-scored-315-in-gre",
        permanent: true,
      },
      {
        source: "/blog_categories/the-best-gre-practice-tests",
        destination: "/best-gre-practice-tests-2022",
        permanent: true,
      },
      {
        source: "/blog_categories/understanding-gre-test-scores",
        destination: "/best-gre-practice-tests-2022",
        permanent: true,
      },
      {
        source: "/blog_categories/understanding-gre-test-scores",
        destination: "/understanding-how-the-gre-is-scored",
        permanent: true,
      },
      {
        source: "/blog_categories/what-is-the-fulbright-selection-criteria",
        destination: "/fulbright-selection-criteria",
        permanent: true,
      },
      {
        source: "/blog_categories/when-should-you-take-the-gre",
        destination: "/when-is-the-right-time-to-take-the-gre",
        permanent: true,
      },
      {
        source: "/blog_categories/tips-for-fulbright-interviews/",
        destination: "/fulbright-interview-experience-tips",
        permanent: true,
      },
      {
        source:
          "/blog_categories/roadmap-guide-to-fulbright-scholarship-2019-20/",
        destination: "/fulbright-scholarship-step-wise-guide/",
        permanent: true,
      },
      {
        source: "/blog_categories/gre-blog/blog_posts/gre-quantitative-guide/",
        destination: "/how-to-study-for-the-gre-math/",
        permanent: true,
      },
      {
        source: "/blog_categories/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/blog_categories/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },

      {
        source: "/gre-guides/best-gre-practice-tests",
        destination: "/best-gre-practice-tests-2022",
        permanent: true,
      },
      {
        source: "/gre-guides/how-hard-is-the-gre",
        destination: "/how-hard-is-the-gre-maths-my-personal-experience",
        permanent: true,
      },
      {
        source:
          "/gre-guides/ideal-gre-scores-of-the-fulbright-selected-candidates",
        destination: "/gre-scores-of-the-fulbright-selected-candidates",
        permanent: true,
      },
      {
        source:
          "/gre-guides/the-best-approach-to-take-gre-practice-tests/www.youtube.com/c/scholarden/",
        destination: "/the-best-approach-to-take-gre-practice-tests/",
        permanent: true,
      },
      {
        source: "/gre-guides/gre-vs-gmat/",
        destination: "/gmat-vs-gre-difference/",
        permanent: true,
      },
      {
        source: "/gre-guides/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/gre-guides/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },

      {
        source:
          "/fulbright-scholarship/fulbright-scholarship-2020-step-wise-guide",
        destination: "/fulbright-scholarship-step-wise-guide",
        permanent: true,
      },
      {
        source: "/fulbright-scholarship/tips-for-fulbright-interviews",
        destination: "/fulbright-interview-experience-tips",
        permanent: true,
      },
      {
        source:
          "/fulbright-scholarship/what-is-the-fulbright-selection-criteria",
        destination: "/fulbright-selection-criteria",
        permanent: true,
      },
      {
        source:
          "/fulbright-scholarship/fulbright-inspiration-story-phd-in-ece/www.facebook.com/scholarden",
        destination: "/fulbright-inspiration-story-phd-in-ece",
        permanent: true,
      },
      {
        source: "/fulbright-scholarship/fulbright-interview",
        destination: "/fulbright-interview-1",
        permanent: true,
      },
      {
        source: "/fulbright-scholarship/fulbright-interview/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/fulbright-scholarship/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/fulbright-scholarship/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },

      {
        source: "/fulbright-interview/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/fulbright-interview/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },

      {
        source: "/fulbright-success-story/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/fulbright-success-story/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },

      {
        source: "/gre-maths/do-good-grades-guarantee-high-gre-quant-scores/",
        destination: "/do-good-grades-guarantee-high-gre-math-scores/",
        permanent: true,
      },
      {
        source: "/gre-maths/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/gre-maths/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },

      {
        source: "/gre-verbal/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/gre-verbal/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },

      {
        source: "/gre-math/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/gre-math/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },

      {
        source: "/gre-success-story/how-i-scored-315/",
        destination: "/how-i-scored-315-in-gre/",
        permanent: true,
      },

      {
        source: "/gre-success-story/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/gre-success-story/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },

      {
        source: "/improve-gre-score/gre-318-abdul-qadir-khan-durrani/",
        destination:
          "/gre-scores-for-civil-and-environmental-engineering-ms-programs/",
        permanent: true,
      },
      {
        source: "/improve-gre-score/gre-320-maryam-ahmad/",
        destination: "/how-to-score-320-in-gre/",
        permanent: true,
      },
      {
        source: "/improve-gre-score/gre-323-syed-farzan-ahmed/",
        destination: "/gre-success-story-323/",
        permanent: true,
      },
      {
        source: "/improve-gre-score/how-i-scored-315/",
        destination: "/how-i-scored-315-in-gre/",
        permanent: true,
      },
      {
        source: "/improve-gre-score/i-scored-324-being-an-engineer/",
        destination: "/how-i-scored-gre-324-in-one-month/",
        permanent: true,
      },
      {
        source: "/improve-gre-score/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/improve-gre-score/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },

      {
        source: "/gre-vocabulary-flashcards/:slug",
        destination: "https://scholarden.com/vocabulary",
        permanent: true,
      },
      {
        source: "/gre-vocabulary-flashcards/:slug/feed",
        destination: "https://scholarden.com/vocabulary",
        permanent: true,
      },
      {
        source: "/college-admissions/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/college-admissions/:slug/feed",
        destination: "/:slug",
        permanent: true,
      },
      {
        source:
          "/scholarships/step-by-step-scholarship-guide-to-the-usa-phd-programs/",
        destination:
          "/complete-step-by-step-guide-for-the-usa-phd-scholarships/",
        permanent: true,
      },
      {
        source:
          "/gre/improve-gre-score/how-to-get-a-perfect-quant-score-in-a-gre-exam/",
        destination: "/gre-330-with-a-perfect-math-score",
        permanent: true,
      },
      {
        source: "/gre/improve-gre-score/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/gre/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source:
          "/phd-success-story-electrical-engineering-ubc-canada-kings-college-uk-anu-uni-of-sydeny-usw-australia-uni-of-luxembourg",
        destination: "/masters-phd-success-story-electrical-engineering",
        permanent: true,
      },
      {
        source: "/phd/:slug",
        destination: "/:slug",
        permanent: true,
      },
      {
        source: "/quiz/:slug",
        destination: "/",
        permanent: true,
      },

      // !! Others
      {
        source: "/category/admissions-scholarships-guides/",
        destination: "/fulbright-vs-direct-admissions-scholarships/",
        permanent: true,
      },
      {
        source: "/blog_categories/best...",
        destination: "/",
        permanent: true,
      },
      {
        source: "/category/fulbright-interview/",
        destination: "/fulbright-interview-experience-tips/",
        permanent: true,
      },
      {
        source: "/category/fulbright-scholarship/",
        destination: "/fulbright-scholarship-step-wise-guide/",
        permanent: true,
      },
      {
        source: "/category/fulbright-scholarship/fulbright-interview/",
        destination: "/fulbright-interview-experience-tips/",
        permanent: true,
      },
      {
        source: "/category/gre-guides/",
        destination:
          "/what-is-the-gre-test-and-why-we-need-it-online-guides-and-tips/",
        permanent: true,
      },
      {
        source: "/category/gre-math/",
        destination: "/gre-math-test-preparation-and-reviews/",
        permanent: true,
      },
      {
        source: "/category/gre-success-story/",
        destination: "/gre-success-story-323/",
        permanent: true,
      },
      {
        source: "/gre-success-story/how-i-scored-315/",
        destination: "/how-i-scored-315-in-gre/",
        permanent: true,
      },
      {
        source: "/category/gre-verbal/",
        destination: "/gre-verbal-study-plan/ ",
        permanent: true,
      },
      {
        source: "/element_category/elements/",
        destination: "/scholar-den-gre-quantitative-diagnostic-test/",
        permanent: true,
      },
      {
        source: "/element_category/sections/",
        destination: "/scholar-den-gre-quantitative-diagnostic-test/",
        permanent: true,
      },
      {
        source: "/page/2/",
        destination: "/",
        permanent: true,
      },
      {
        source: "/tag/gre-concepts/",
        destination: "/",
        permanent: true,
      },
      {
        source: "/tag/gre-success-story/",
        destination: "/",
        permanent: true,
      },
      {
        source: "/wp-content/uploads/2020/02/blog-post-1680-",
        destination: "/",
        permanent: true,
      },
      {
        source: "/wp-content/uploads/2022/01/blog-post-1680-",
        destination: "/",
        permanent: true,
      },
      {
        source: "/wp-content/uploads/2022/01/Untitled-1680-",
        destination: "/",
        permanent: true,
      },
      {
        source: "/uncategorized/how-to-do-the-gre-registration/",
        destination: "/how-to-do-the-gre-registration/",
        permanent: true,
      },
      {
        source: "/tips-for-fulbright-interviews/feed/",
        destination: "/fulbright-interview-success-story/",
        permanent: true,
      },
      {
        source: "/tag/gre-scores/",
        destination: "/understanding-how-the-gre-is-scored/",
        permanent: true,
      },
      {
        source: "/tag/best-gre-books/",
        destination: "/the-5-best-books-when-preparing-for-the-gre/",
        permanent: true,
      },
      {
        source: "/how-to-score-329-in-gre/feed/",
        destination: "/how-i-score-329-in-gre/",
        permanent: true,
      },
      {
        source: "/element_category/sections/",
        destination: "/scholar-den-gre-quantitative-diagnostic-test/",
        permanent: true,
      },
      {
        source: "/element_category/elements/",
        destination: "/scholar-den-gre-quantitative-diagnostic-test/",
        permanent: true,
      },
    ]
  },
}

module.exports = nextConfig
