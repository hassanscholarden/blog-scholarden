/* eslint-disable @typescript-eslint/naming-convention */
import { WP_BLOG_POSTS_API_ENDPOINT } from "constants/endpoints"
import { fetchData } from "handlers"
import type {
  getCategoriesPostsDataType,
  getFeaturedPostsDataType,
} from "types/handlers/homPage"
import type { CategoryPostSection } from "types/home"
import type { WpPost } from "types/wordpress-response/post"
import { flattenCategories } from "utils/blogs/categoryFormatters"
import { formatPostCard } from "utils/blogs/postFormatter"

export const getFeaturedPostsData: getFeaturedPostsDataType = async () => {
  try {
    const resp: WpPost[] = await fetchData(
      `${WP_BLOG_POSTS_API_ENDPOINT}?featured=true&per_page=7&_embed=true`
    )

    const featuredPosts = formatPostCard(resp ?? [])

    const response = {
      featuredPosts: featuredPosts[0] ?? null,
      topPosts: featuredPosts.slice(1, 7),
    }

    return response
  } catch (error) {
    return {
      featuredPosts: null,
      topPosts: [],
    }
  }
}

export const getCategoriesPostsSectionsData: getCategoriesPostsDataType =
  async (categoriesWithSubcategories) => {
    const categories = flattenCategories(categoriesWithSubcategories, true)
    try {
      const categoriesPostsPromises: Array<Promise<WpPost[]>> = []

      const getCategoryPostsEndpoint = (id: number): string => {
        return `${WP_BLOG_POSTS_API_ENDPOINT}?categories=${id}&featured=true&per_page=4&_embed=true`
      }

      const pushId = (id: number): void => {
        const categoryPostsPromise: Promise<WpPost[]> = fetchData(
          getCategoryPostsEndpoint(id)
        )
        categoriesPostsPromises.push(categoryPostsPromise)
      }

      categories.forEach((category) => {
        const { id } = category

        pushId(id)
      })

      const categoriesPostsList = await Promise.all(categoriesPostsPromises)

      const categoriesPostSections: CategoryPostSection[] = []

      categories.forEach((category, index) => {
        const WpPosts = categoriesPostsList[index]

        if (Array.isArray(WpPosts)) {
          const posts = formatPostCard(WpPosts)
          categoriesPostSections.push({
            ...category,
            posts,
          })
        }
      })

      return categoriesPostSections
    } catch (error) {
      return []
    }
  }
