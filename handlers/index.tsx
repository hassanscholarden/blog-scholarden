import {
  WP_BLOG_CATEGORIES_API_ENDPOINT,
  WP_BLOG_POSTS_API_ENDPOINT,
} from "constants/endpoints"
import type { Category } from "types/category"
import type {
  getCategoriesListDataType,
  getSlugsListDataType,
} from "types/handlers/homPage"
import { type WpSlugsListType } from "types/wordpress-response"
import type { WpCategory } from "types/wordpress-response/categories"
import { formateCategory } from "utils/blogs/categoryFormatters"
import { replaceDevelopUrlToBlogUrl } from "utils/strings"

export const fetchData: (url: string) => Promise<any[]> = async (url) => {
  const response = await fetch(url, {
    next: { revalidate: 900 },
  })
  const data: any[] = await response.json()

  return replaceDevelopUrlToBlogUrl(data)
}

export const getCategoriesListData: getCategoriesListDataType = async () => {
  try {
    const wpCategories: WpCategory[] = await fetchData(
      `${WP_BLOG_CATEGORIES_API_ENDPOINT}?per_page=100&parent=0`
    )

    const categoriesWithSubcategories: Category[] = []

    for (let index = 0; index < wpCategories.length; index++) {
      const wpCategory = wpCategories[index]

      if (wpCategory.name.toLocaleLowerCase() === "blog_categories") {
        continue
      }

      const category = formateCategory(wpCategory)

      if (category.parent === 0) {
        const wpSubcategories: WpCategory[] = await fetchData(
          `${WP_BLOG_CATEGORIES_API_ENDPOINT}?per_page=100&parent=${category.id}`
        )

        if (Array.isArray(wpSubcategories) && wpSubcategories.length > 0) {
          category.subcategories = wpSubcategories.map((el) =>
            formateCategory(el)
          )
        }
      }

      categoriesWithSubcategories.push(category)
    }

    return await Promise.all(categoriesWithSubcategories)
  } catch (error) {
    // console.error("Error fetching categories:", error)
    return []
  }

  // try {
  //   const resp: WpCategory[] = await fetchData(
  //     `${WP_BLOG_CATEGORIES_API_ENDPOINT}`
  //   )
  //   const categories = formatCategoriesList(Array.isArray(resp) ? resp : [])

  //   return categories
  // } catch (error) {
  //   return []
  // }
}

export const getAllCategoriesSlugs: getSlugsListDataType = async () => {
  try {
    const resp: WpSlugsListType = await fetchData(
      `${WP_BLOG_CATEGORIES_API_ENDPOINT}?_fields=slug&per_page=100`
    )

    const slugs = resp.map((item) => item.slug)

    return slugs
  } catch (error) {
    return []
  }
}
export const getAllPostsSlugs: getSlugsListDataType = async () => {
  try {
    const resp: WpSlugsListType = await fetchData(
      `${WP_BLOG_POSTS_API_ENDPOINT}?_fields=slug&per_page=100`
    )

    const slugs = resp.map((item) => item.slug)

    return slugs
  } catch (error) {
    return []
  }
}
