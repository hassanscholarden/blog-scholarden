import { WP_BLOG_COMMENTS_API_ENDPOINT } from "constants/endpoints"
import { fetchData } from "handlers"
import type { Comment } from "types/comments"
import type {
  getCommentsListHandlerType,
  postCommentHandlerType,
} from "types/handlers/comments"
import type { WpComment } from "types/wordpress-response/comments"
import { formatComment } from "utils/blogs/commentsFormatters"

export const postCommentHandler: postCommentHandlerType = async (
  commentData
) => {
  let message = "Something went wrong while submitting your thought"
  let type: "success" | "error" = "error"
  let comment: Comment | null = null
  try {
    const response = await fetch(WP_BLOG_COMMENTS_API_ENDPOINT, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(commentData),
    })

    if (response.ok) {
      const wpComment: WpComment = await response.json()

      comment = formatComment(wpComment)
      message = "Comment Submitted for Admin Approval"
      type = "success"
    } else {
      const serverResponse = await response.json()

      message = serverResponse?.message ?? message
      type = "error"
    }

    return {
      comment,
      type,
      message,
    }
  } catch (error) {
    type = "error"

    return {
      comment: null,
      type,
      message,
    }
  }
}

export const getCommentsListHandler: getCommentsListHandlerType = async (
  postId,
  page,
  perPage = 5
) => {
  try {
    const resp: WpComment[] = await fetchData(
      WP_BLOG_COMMENTS_API_ENDPOINT +
        `?post=${String(postId)}&per_page=${perPage}&page=${page}&parent=0`
    )

    const commentsWithReplies = resp.map(async (repliesData) => {
      const comment = formatComment(repliesData)

      if (comment.parent === 0) {
        const repliesResponse: WpComment[] = await fetchData(
          WP_BLOG_COMMENTS_API_ENDPOINT +
            `?per_page=10&page=1&parent=${comment.id}`
        )

        const replies = repliesResponse.map((el) => formatComment(el))
        comment.replies = replies
      }

      return comment
    })

    const list = await Promise.all(commentsWithReplies)

    let hasMoreComments = false

    if (resp.length < perPage) {
      hasMoreComments = false
    } else {
      hasMoreComments = true
    }

    return { hasMoreComments, list }
  } catch (error) {
    return { hasMoreComments: false, list: [] }
  }
}
