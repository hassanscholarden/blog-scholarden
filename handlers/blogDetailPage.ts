/* eslint-disable @typescript-eslint/naming-convention */

import { WP_BLOG_POSTS_API_ENDPOINT } from "constants/endpoints"
import { type PostDetails } from "types/postCards"
import { type WpPost } from "types/wordpress-response/post"
import { formatPostDetails } from "utils/blogs/postFormatter"
import { replaceDevelopUrlToBlogUrl } from "utils/strings"

const fetchPosts: (url: string) => Promise<WpPost[]> = async (url) => {
  const response = await fetch(url)
  const data: WpPost[] = await response.json()

  return replaceDevelopUrlToBlogUrl(data)
}

type getPostDetailsType = (slug: string) => Promise<PostDetails | null>

export const getPostDetails: getPostDetailsType = async (slug) => {
  const resp = await fetchPosts(
    `${WP_BLOG_POSTS_API_ENDPOINT}?_embed=true&slug=${slug}`
  )

  const details = formatPostDetails(resp)

  return details
}
