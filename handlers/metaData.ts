import {
  WP_BLOG_CATEGORIES_API_ENDPOINT,
  WP_BLOG_PAGES_API_ENDPOINT,
  WP_BLOG_POSTS_API_ENDPOINT,
} from "constants/endpoints"
import { fetchData } from "handlers"
import type {
  getHomePageYoastHeadJsonType,
  getYoastHeadJsonHandlerType,
  getYoastHeadJsonHandlerUsingSlugType,
} from "types/handlers/metaData"
import { replaceDevelopUrlToBlogUrl } from "utils/strings"

export const getYoastHeadJsonHandler: getYoastHeadJsonHandlerType = async (
  endpoint,
  slug
) => {
  try {
    const response = await fetchData(
      endpoint + `?slug=${slug}&_fields=yoast_head_json`
    )

    const category = response[0] ?? {}

    const yoastHeadJson = category.yoast_head_json ?? {}

    const meta = replaceDevelopUrlToBlogUrl(yoastHeadJson)

    return meta
  } catch (error) {}
}
export const getCategoryYoastHeadJson: getYoastHeadJsonHandlerUsingSlugType =
  async (slug) => {
    try {
      const response = await getYoastHeadJsonHandler(
        WP_BLOG_CATEGORIES_API_ENDPOINT,
        slug
      )

      return response
    } catch (error) {
      return {}
    }
  }

export const getPostYoastHeadJson: getYoastHeadJsonHandlerUsingSlugType =
  async (slug) => {
    try {
      const response = await getYoastHeadJsonHandler(
        WP_BLOG_POSTS_API_ENDPOINT,
        slug
      )

      return response
    } catch (error) {
      return {}
    }
  }

export const getHomePageYoastHeadJson: getHomePageYoastHeadJsonType =
  async () => {
    try {
      const response = await getYoastHeadJsonHandler(
        WP_BLOG_PAGES_API_ENDPOINT,
        "blogs"
      )

      return response
    } catch (error) {
      return {}
    }
  }
