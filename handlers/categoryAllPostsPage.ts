import { WP_BLOG_POSTS_API_ENDPOINT } from "constants/endpoints"
import { fetchData } from "handlers"
import type {
  getAllPostsOfCategoriesType,
  getFeaturedPostsOfCategoryDataType,
  getPostsOfCategoryType,
} from "types/handlers/categoryAllPostsPage"
import type { WpPost } from "types/wordpress-response/post"
import { formatPostCard } from "utils/blogs/postFormatter"

export const getFeaturedPostsOfCategoryData: getFeaturedPostsOfCategoryDataType =
  async (categoryId) => {
    try {
      const resp: WpPost[] = await fetchData(
        `${WP_BLOG_POSTS_API_ENDPOINT}?categories=${categoryId}&featured=true&per_page=4&_embed=true`
      )

      const featuredPosts = formatPostCard(resp ?? [])

      const response = {
        featuredPosts: featuredPosts[0] ?? null,
        topPosts: featuredPosts.slice(1, 4),
      }

      return response
    } catch (error) {
      return {
        featuredPosts: null,
        topPosts: [],
      }
    }
  }

export const getAllPostsOfCategories: getAllPostsOfCategoriesType = async (
  id,
  page,
  perPage
) => {
  const defaultResp = {
    posts: [],
    page,
    perPage,
    hasMore: false,
  }
  try {
    const resp: WpPost[] = await fetchData(
      `${WP_BLOG_POSTS_API_ENDPOINT}?_embed=true&categories=${id}&featured=false&page=${page}&per_page=${perPage}`
    )

    const posts = formatPostCard(resp)
    const hasMore = posts.length === perPage

    return {
      posts,
      page,
      perPage,
      hasMore,
    }
  } catch (error) {
    return defaultResp
  }
}

export const getPostsOfCategory: getPostsOfCategoryType = async (params) => {
  const { categoryIds, featured = true, perPage = 5 } = params
  const categories = categoryIds.join(",")

  try {
    const resp: WpPost[] = await fetchData(
      `${WP_BLOG_POSTS_API_ENDPOINT}?categories=${categories}&featured=${
        featured ? "true" : "false"
      }&per_page=${perPage}&_embed=true`
    )

    const posts = formatPostCard(resp ?? [])

    return posts
  } catch (error) {
    return []
  }
}
