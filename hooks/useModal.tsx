import { useCallback, useState } from "react"

type ModalState = boolean
type ToggleModal = () => void
type ModalDispatcher = React.Dispatch<React.SetStateAction<ModalState>>

/**
 * Hook for managing the state of a modal.
 * @param initialMode - The initial state of the modal.
 * @returns A tuple containing the modal state, setModalOpen function, and toggle function.
 */
const useModal = (
  initialMode: ModalState = false
): [ModalState, ToggleModal, ModalDispatcher] => {
  const [modalOpen, setModalOpen] = useState<ModalState>(initialMode)
  const toggle = useCallback(() => {
    setModalOpen((prevModalOpen) => !prevModalOpen)
  }, [])

  return [modalOpen, toggle, setModalOpen]
}
export default useModal
