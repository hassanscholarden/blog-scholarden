export interface SearchPostType {
  id: number
  title: string
  url: string
  type: string
  subtype: string
}
