export interface Comment {
  id: number
  post: number
  parent: number
  author: number
  authorName: string
  authorUrl: string
  date: number
  content: string
  authorAvatarUrl: string | null
  replies: Comment[]
}

export type getPostsCommentsType = () => Promise<Comment[]>

export type createCommentType = (
  event: React.FormEvent<HTMLFormElement>
) => Promise<any>

export interface postCommentDefaultAlertMessageType {
  show: boolean
  message: string
  type: "success" | "error"
}

export type appendCommentInListType = (
  comment: Comment,
  parentId?: number
) => void
