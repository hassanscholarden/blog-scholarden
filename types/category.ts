interface CategoryType {
  id: number
  count: number
  description: string
  name: string
  slug: string
  taxonomy: string
  parent: number
}
export interface Category extends CategoryType {
  subcategories?: CategoryType[]
}
