export type handlerType = () => void

export type formEventType = (
  event: React.FormEvent<HTMLFormElement>
) => Promise<any>

export type mouseEventHandlerType = (
  e: React.MouseEvent<HTMLButtonElement, MouseEvent>
) => void

export type promiseHandlerType = () => Promise<any>
