import type { Comment } from "types/comments"

interface postCommentHandlerParamsType {
  post: number
  author_name: string
  author_email: string
  content: string
}

interface postCommentHandlerReturnType {
  comment: Comment | null
  message: string
  type: "success" | "error"
}
export type postCommentHandlerType = (
  commentData: postCommentHandlerParamsType
) => Promise<postCommentHandlerReturnType>

export type getCommentsListHandlerType = (
  postId: number,
  page: number,
  perPage?: number
) => Promise<{ hasMoreComments: boolean; list: Comment[] }>
