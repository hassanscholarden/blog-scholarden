import type { PostCard } from "types/postCards"

export type getFeaturedPostsOfCategoryDataType = (
  categoryId: number
) => Promise<{
  featuredPosts: PostCard | null
  topPosts: PostCard[]
}>
export type getPostsOfCategoryType = (params: {
  categoryIds: number[]
  featured?: boolean
  perPage?: number
}) => Promise<PostCard[]>

export type getAllPostsOfCategoriesType = (
  categoryId: number,
  page: number,
  perPage: number
) => Promise<{
  posts: PostCard[]
  page: number
  perPage: number
  hasMore: boolean
}>
