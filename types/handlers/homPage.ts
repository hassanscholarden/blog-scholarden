import type { Category } from "types/category"
import type { CategoryPostSection } from "types/home"
import type { PostCard } from "types/postCards"

export type getFeaturedPostsDataType = () => Promise<{
  featuredPosts: PostCard | null
  topPosts: PostCard[]
}>

export type getCategoriesListDataType = () => Promise<Category[]>

export type getSlugsListDataType = () => Promise<string[]>

export type getCategoriesPostsDataType = (
  categoriesList: Category[]
) => Promise<CategoryPostSection[]>
