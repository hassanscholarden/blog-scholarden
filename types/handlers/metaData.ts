import type { Metadata } from "next"
// import type { YoastHeadJsonType } from "types/wordpress-response"

export type getYoastHeadJsonHandlerType = (
  endpoint: string,
  slug: string
) => Promise<Metadata>

export type getYoastHeadJsonHandlerUsingSlugType = (
  slug: string
) => Promise<Metadata>
export type getHomePageYoastHeadJsonType = () => Promise<Metadata>
