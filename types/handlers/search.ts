export interface WpSearchPostType {
  id: number
  title: string
  url: string
  type: string
  subtype: string
  _links: {
    self: Array<{
      embeddable: boolean
      href: string
    }>
    about: Array<{
      href: string
    }>
    collection: Array<{
      href: string
    }>
  }
}
