import type { YoastHeadJsonType } from "."

export interface WpCategory {
  id: number
  count: number
  description: string
  link: string
  name: string
  slug: string
  taxonomy: string
  parent: number
  meta: any[]
  yoast_head: string
  yoast_head_json: YoastHeadJsonType
}
