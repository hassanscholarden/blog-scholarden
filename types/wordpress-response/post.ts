import { type YoastHeadJsonType } from "."

export interface WpPost {
  id: number
  date: string
  date_gmt: string
  guid: {
    rendered: string
  }
  modified: string
  modified_gmt: string
  slug: string
  status: string
  type: string
  link: string
  title: {
    rendered: string
  }
  content: {
    rendered: string
    protected: boolean
  }
  excerpt: {
    rendered: string
    protected: boolean
  }
  author: number
  featured_media: number
  comment_status: string
  ping_status: string
  sticky: boolean
  template: string
  format: string
  meta: any[]
  categories: number[]
  tags: number[]
  yoast_head: string
  yoast_head_json: YoastHeadJsonType
  _embedded: {
    author: WpPostAuthor[]
    "wp:featuredmedia": WpPostFeaturedmedia[]
    "wp:term": WpPostTerm[]
  }
}

export interface WpPostAuthor {
  id: number
  name: string
  url: string
  description: string
  link: string
  slug: string
  avatar_urls: {
    24: string
    48: string
    96: string
  }
  yoast_head: string
  yoast_head_json: YoastHeadJsonType
  _links: {
    self: Array<{
      href: string
    }>
    collection: Array<{
      href: string
    }>
  }
}

export interface WpPostFeaturedmedia {
  id: number
  date: string
  slug: string
  type: string
  link: string
  title: {
    rendered: string
  }
  author: number
  caption: {
    rendered: string
  }
  alt_text: string
  media_type: string
  mime_type: string
  media_details: {
    width: number
    height: number
    file: string
    sizes: Record<
      string,
      {
        file: string
        width: number
        height: number
        mime_type: string
        source_url: string
      }
    >
    image_meta: {
      aperture: string
      credit: string
      camera: string
      caption: string
      created_timestamp: string
      copyright: string
      focal_length: string
      iso: string
      shutter_speed: string
      title: string
      orientation: string
      keywords: string[]
      resized_images: Record<string, string>
    }
  }
  source_url: string
  _links: {
    self: Array<{
      href: string
    }>
    collection: Array<{
      href: string
    }>
    about: Array<{
      href: string
    }>
    author: Array<{
      embeddable: boolean
      href: string
    }>
    replies: Array<{
      embeddable: boolean
      href: string
    }>
  }
}

export type WpPostTerm = WpPostTermDataType[]
export interface WpPostTermDataType {
  id: number
  link: string
  name: string
  slug: string
  taxonomy: "category" | "post_tag"
  yoast_head: string
  yoast_head_json: YoastHeadJsonType
}

// export interface WpPostCategories {
//   id: number
//   link: string
//   name: string
//   slug: string
//   taxonomy: string
//   yoast_head: string
//   yoast_head_json: {
//     title: string
//     robots: {
//       index: string
//       follow: string
//     }
//     og_locale: string
//     og_type: string
//     og_title: string
//     og_url: string
//     og_site_name: string
//     twitter_card: string
//     schema: {
//       "@context": string
//       "@graph": Array<{
//         "@type": string
//         "@id": string
//         url: string
//         name: string
//         isPartOf: string
//         breadcrumb: string
//         inLanguage: string
//       }>
//     }
//   }
//   _links: {
//     self: Array<{
//       href: string
//     }>
//     collection: Array<{
//       href: string
//     }>
//     about: Array<{
//       href: string
//     }>
//     "wp:post_type": Array<{
//       href: string
//     }>
//     curies: Array<{
//       name: string
//       href: string
//       templated: boolean
//     }>
//   }
// }

// export interface WpPostTags {
//   id: number
//   link: string
//   name: string
//   slug: string
//   taxonomy: string
//   yoast_head: string
//   yoast_head_json: {
//     title: string
//     robots: {
//       index: string
//       follow: string
//       "max-snippet": string
//       "max-image-preview": string
//       "max-video-preview": string
//     }
//     canonical: string
//     og_locale: string
//     og_type: string
//     og_title: string
//     og_url: string
//     og_site_name: string
//     twitter_card: string
//     schema: {
//       "@context": string
//       "@graph": Array<{
//         "@type": string
//         "@id": string
//         url: string
//         name: string
//         isPartOf: string
//         breadcrumb: string
//         inLanguage: string
//       }>
//     }
//   }
//   _links: {
//     self: Array<{
//       href: string
//     }>
//     collection: Array<{
//       href: string
//     }>
//     about: Array<{
//       href: string
//     }>
//     "wp:post_type": Array<{
//       href: string
//     }>
//     curies: Array<{
//       name: string
//       href: string
//       templated: boolean
//     }>
//   }
// }
