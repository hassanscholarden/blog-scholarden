export interface YoastHeadJsonType {
  title: string
  description: string
  robots: {
    index: string
    follow: string
    "max-snippet": string
    "max-image-preview": string
    "max-video-preview": string
  }
  canonical: string
  og_locale: string
  og_type: string
  og_title: string
  og_description: string
  og_url: string
  og_site_name: string
  article_publisher: string
  article_published_time: string
  article_modified_time: string
  og_image: Array<{
    width: number
    height: number
    url: string
    type: string
  }>
  author: string
  twitter_card: string
  twitter_misc: Record<string, string>
  schema: {
    "@context": string
    "@graph": Array<{
      "@type": string
      "@id": string
      isPartOf: {
        "@id": string
      }
      author: {
        name: string
        "@id": string
      }
      headline: string
      datePublished: string
      dateModified: string
      mainEntityOfPage: {
        "@id": string
      }
      wordCount: number
      commentCount: number
      publisher: {
        "@id": string
      }
      keywords: string[]
      articleSection: string[]
      inLanguage: string
      potentialAction: Array<{
        "@type": string
        name: string
        target: string[]
      }>
    }>
  }
}

export type WpSlugsListType = Array<{
  slug: string
}>
