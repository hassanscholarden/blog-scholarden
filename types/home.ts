import type { Category } from "./category"
import type { PostCard } from "./postCards"

export interface CategoryPostSection extends Category {
  posts: PostCard[]
}
export interface HomePageDataType {
  featuredPosts: PostCard | null
  topPosts: PostCard[]
  categories: CategoryPostSection[]
}
