import { type YoastHeadJsonType } from "./wordpress-response"

export type postStatusType = "publish" | "unpublish"

export interface PostDetails extends PostCard {
  content: string
  yoastHeadJson: YoastHeadJsonType
}
export interface PostCard {
  id: number
  date: number
  modified: number
  slug: string
  status: string
  type: string
  title: string
  excerpt: string
  author: PostAuthor
  featuredMedia: PostFeaturedMedia | null
  categories: PostCategory[]
  tags: PostTags[]
  readingTime: string
  commentCount: number
  yoastHead: string
}

export interface PostAuthor {
  id: number
  name: string
  description: string
  slug: string
  avatarUrl: string
}

export interface PostFeaturedMedia {
  id: number
  date: number
  slug: string
  altText: string
  author: number
  url: string
}

interface PostTerm {
  id: number
  name: string
  slug: string
}

export interface PostCategory extends PostTerm {
  taxonomy: "category"
}

export interface PostTags extends PostTerm {
  taxonomy: "post_tag"
}
