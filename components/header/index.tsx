/* eslint-disable react/no-multi-comp */
/* eslint-disable jsx-a11y/anchor-is-valid */
"use client"

import React, { type FC, useState, useEffect } from "react"

import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline"
import {
  Navbar,
  Collapse,
  Typography,
  IconButton,
} from "@material-tailwind/react"
import Image from "next/image"
import Link from "next/link"

import { navLinks } from "constants/navItems"
import Logo from "public/logo.png"

function NavList(): any {
  return (
    <ul className="my-2 flex flex-col space-y-2 lg:my-0 lg:flex-row lg:items-center lg:space-x-6">
      {navLinks.map((nav, index) => (
        <Typography
          key={index}
          as="li"
          variant="large"
          className="p-1 font-medium text-gray-900"
        >
          <a
            href={nav.url}
            className="flex items-center transition-colors hover:text-primary-500"
            target="_blank"
          >
            {nav.title}
          </a>
        </Typography>
      ))}
    </ul>
  )
}

const Header: FC = () => {
  const [openNav, setOpenNav] = useState(false)

  const handleWindowResize: any = () => {
    window.innerWidth >= 960 && setOpenNav(false)
  }

  useEffect(() => {
    window.addEventListener("resize", handleWindowResize)

    return () => {
      window.removeEventListener("resize", handleWindowResize)
    }
  }, [])

  return (
    <Navbar className="cont mx-auto max-w-screen-xl">
      <div className="flex items-center justify-between text-gray-900">
        <Link
          href="/"
          className="mr-4 w-[140px] cursor-pointer py-1.5 sm:w-[150px] md:w-[165px]"
        >
          <Image src={Logo} alt="scholarden" priority />
        </Link>

        <div className="hidden lg:block">
          <NavList />
        </div>

        <IconButton
          variant="text"
          className="ml-auto h-6 w-6 text-inherit hover:bg-transparent focus:bg-transparent active:bg-transparent lg:hidden"
          ripple={false}
          onClick={() => {
            setOpenNav(!openNav)
          }}
          name="toggle menu"
          aria-label="toggle menu"
          aria-labelledby="toggle menu"
        >
          {openNav ? (
            <XMarkIcon className="h-6 w-6" strokeWidth={2} />
          ) : (
            <Bars3Icon className="h-6 w-6" strokeWidth={2} />
          )}
        </IconButton>
      </div>
      <Collapse open={openNav}>
        <NavList />
      </Collapse>
    </Navbar>
  )
}

export default Header

// /* eslint-disable jsx-a11y/anchor-is-valid */
// import React, { type FC } from "react"

// import Image from "next/image"

// import Button from "@core/button"
// import Logo from "public/logo.png"

// const Header: FC = () => {
//   return (
//     <div className="sticky top-0 bg-white">

//       <div className="flex border border-solid border-gray-300 px-20 py-5">
//         <div className="w-44">
//           <Image src={Logo} alt="scholarden" />
//         </div>

//         <div className="flex flex-1 justify-end">
//           <div className="flex space-x-5">
//             <Button variant="text" name="Dashboard">Dashboard</Button>

//             <Button variant="text" name="Home">Home</Button>

//             <Button variant="text" name="Pricing">Pricing</Button>
//           </div>
//         </div>
//       </div>
//     </div>
//   )
// }

// export default Header
