import * as React from "react"

import Image from "next/image"
import Link from "next/link"

import MTAvatar from "@core/MaterialTailwind/Avatar"
import imagePlaceholder from "public/images/placeholder.png"
import type { PostCard } from "types/postCards"
import { formatDate } from "utils/date"

interface Props {
  data: PostCard
}

const PostCardV1: React.FC<Props> = ({ data }) => {
  const { title, slug, date, featuredMedia, categories, author } = data

  return (
    <Link href={`/${slug}`}>
      <div className="grid cursor-pointer grid-cols-3">
        <div className="relative col-span-1 mb-4 h-full w-full overflow-hidden rounded-[15px]">
          <Image
            className="object-cover"
            src={featuredMedia?.url ?? imagePlaceholder}
            alt={title}
            fill
          />
        </div>

        <div className="col-span-2 flex flex-1 flex-col justify-between space-y-2 px-4 md:space-y-4">
          <div className="space-y-2 md:space-y-4">
            <p
              className="text-[12px] font-semibold text-primary-600 md:text-[15px]"
              dangerouslySetInnerHTML={{ __html: categories[0]?.name ?? "" }}
            />

            <p
              className="text-[15px] font-medium md:text-[17px]"
              dangerouslySetInnerHTML={{ __html: title }}
            />
          </div>

          <div className="flex items-center gap-1 md:gap-3">
            <MTAvatar
              src={author.avatarUrl}
              alt={author.name}
              className="post-card-author-avatar"
            />

            <div className="flex items-center space-x-2 text-[12px] text-gray-750 md:text-[13px]">
              <span className="font-semibold">{author.name}</span>
              <span>|</span>
              <span
                dangerouslySetInnerHTML={{
                  __html: formatDate(date, "mmm, yy"),
                }}
                className="font-medium"
              />
            </div>
          </div>
        </div>
      </div>
    </Link>
  )
}

export default PostCardV1
