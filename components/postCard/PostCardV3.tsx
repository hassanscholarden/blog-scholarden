import * as React from "react"

import Image from "next/image"
import Link from "next/link"

import MTAvatar from "@core/MaterialTailwind/Avatar"
import imagePlaceholder from "public/images/placeholder.png"
import type { PostCard } from "types/postCards"
import { formatDate } from "utils/date"

interface Props {
  data: PostCard
}

const PostCardV3: React.FC<Props> = ({ data }) => {
  const { title, slug, date, featuredMedia, author } = data

  return (
    <Link href={`/${slug}`}>
      <div className="cursor-pointer">
        <div className="relative mb-4 h-[197px] w-full overflow-hidden rounded-[15px]">
          <Image
            className="object-cover"
            src={featuredMedia?.url ?? imagePlaceholder}
            alt={title}
            fill
          />
        </div>

        <div className="col-span-2 mt-8 flex flex-1 flex-col justify-between space-y-4 px-4">
          <div className="space-y-4">
            <p
              className="text-[17px] font-medium"
              dangerouslySetInnerHTML={{ __html: title }}
            />
          </div>

          <div className="flex items-center space-x-3">
            <MTAvatar src={author.avatarUrl} alt={author.name} size="sm" />

            <div className="flex items-center space-x-2 text-[13px]">
              <span className="font-semibold text-gray-750">{author.name}</span>
              <span className="text-gray-750">|</span>
              <span
                dangerouslySetInnerHTML={{
                  __html: formatDate(date, "html dd month, yy"),
                }}
                className="font-medium text-gray-750"
              />
            </div>
          </div>
        </div>
      </div>
    </Link>
  )
}

export default PostCardV3
