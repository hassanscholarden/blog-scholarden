import * as React from "react"

import PostCardV1 from "./PostCardV1"
import PostCardV2 from "./PostCardV2"
import PostCardV3 from "./PostCardV3"
import PostCardV4 from "./PostCardV4"
import PostCardV5 from "./PostCardV5"
import type { PostCard as PostCardType } from "types/postCards"

interface Props {
  data: PostCardType
  variant: "v1" | "v2" | "v3" | "v4" | "v5"
}

const PostCard: React.FC<Props> = ({ variant, data }) => {
  if (variant === "v1") {
    return <PostCardV1 data={data} />
  }
  if (variant === "v2") {
    return <PostCardV2 data={data} />
  }
  if (variant === "v3") {
    return <PostCardV3 data={data} />
  }
  if (variant === "v4") {
    return <PostCardV4 data={data} />
  }
  if (variant === "v5") {
    return <PostCardV5 data={data} />
  }

  return <div />
}

export default PostCard
