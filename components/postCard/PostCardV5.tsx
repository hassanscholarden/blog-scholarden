import * as React from "react"

import Image from "next/image"
import Link from "next/link"

import Icon from "@core/icon"
import MTAvatar from "@core/MaterialTailwind/Avatar"
import imagePlaceholder from "public/images/placeholder.png"
import type { PostCard } from "types/postCards"
import { formatReadingTime, formatDate } from "utils/date"

interface Props {
  data: PostCard
}

const PostCardV5: React.FC<Props> = ({ data }) => {
  const {
    title,
    slug,
    date,
    featuredMedia,
    author,
    readingTime,
    commentCount,
  } = data

  const authorAvatar = (
    <div className="flex items-center gap-3">
      <MTAvatar
        src={author.avatarUrl}
        alt={author.name}
        className="post-card-author-avatar"
      />

      <span className="text-[12px] font-semibold text-gray-750 md:text-[17px]">
        {author.name}
      </span>
    </div>
  )

  return (
    <Link href={`/${slug}`}>
      <div className="grid h-full cursor-pointer grid-cols-3">
        <div className="relative col-span-1 mb-4 h-full min-h-[150px] w-full overflow-hidden rounded-[15px]">
          <Image
            className="object-cover"
            src={featuredMedia?.url ?? imagePlaceholder}
            alt={title}
            fill
          />
        </div>

        <div className="col-span-2 flex flex-1 flex-col justify-between space-y-2 px-4 md:space-y-4 md:py-8">
          <div className="space-y-4">
            <div>{authorAvatar}</div>

            <p
              className="text-[15px] font-semibold sm:text-[26px] md:text-[32px] md:font-medium"
              dangerouslySetInnerHTML={{ __html: title }}
            />
          </div>

          <span className="h-[1px] w-full bg-gray-400" />

          <div className="flex items-center justify-between">
            <div className="flex space-x-2 text-[12px] font-medium text-gray-750 md:space-x-4  md:text-[17px]">
              <p
                dangerouslySetInnerHTML={{
                  __html: formatDate(date, "mmm dd, yyyy"),
                }}
              />
              <span>|</span>
              <span>{formatReadingTime(readingTime, "mins read")}</span>
            </div>

            <div className="hidden items-center justify-center space-x-2 md:flex">
              <span>
                <Icon
                  iconName="comment"
                  className="h-[15px] w-[15px] md:h-[23px] md:w-[23px]"
                />
              </span>

              <span className="text-[17px] text-gray-800">{commentCount}</span>
            </div>
          </div>
        </div>
      </div>
    </Link>
  )
}

export default PostCardV5
