import * as React from "react"

import Image from "next/image"
import Link from "next/link"

import MTAvatar from "@core/MaterialTailwind/Avatar"
import imagePlaceholder from "public/images/placeholder.png"
import type { PostCard } from "types/postCards"
import { formatReadingTime, formatDate } from "utils/date"

interface Props {
  data: PostCard
}

const PostCardV2: React.FC<Props> = ({ data }) => {
  const { title, slug, date, featuredMedia, author, readingTime } = data

  const authorAvatar = (
    <div className="flex items-center gap-2 md:gap-3">
      <MTAvatar
        src={author.avatarUrl}
        alt={author.name}
        className="post-card-author-avatar"
      />

      <span className="text-[12px] font-semibold text-gray-750 md:text-[15px]">
        {author.name}
      </span>
    </div>
  )

  return (
    <Link href={`/${slug}`}>
      <div className="grid h-full cursor-pointer grid-cols-3">
        <div className="relative col-span-1 mb-4 h-full w-full overflow-hidden rounded-[15px]">
          <Image
            className="object-cover"
            src={featuredMedia?.url ?? imagePlaceholder}
            alt={title}
            fill
          />
        </div>

        <div className="col-span-2 flex flex-1 flex-col justify-between space-y-2 px-4 md:space-y-4">
          <div className="space-y-1 sm:space-y-2 md:space-y-4">
            <div className="hidden space-x-2 text-[12px] font-medium text-gray-750 md:flex md:text-[15px]">
              <p
                dangerouslySetInnerHTML={{
                  __html: formatDate(date, "html dd month, yy"),
                }}
              />
              <span> | </span>

              <span>{formatReadingTime(readingTime, "mins read")}</span>
            </div>

            <div className="inline-block md:hidden">{authorAvatar}</div>

            <p
              className="text-[15px] font-medium md:text-[23px]"
              dangerouslySetInnerHTML={{ __html: title }}
            />
          </div>

          <div className="hidden md:inline-block">{authorAvatar}</div>

          <div className="flex space-x-2 text-[12px] font-medium  text-gray-750 md:hidden">
            <p
              dangerouslySetInnerHTML={{
                __html: formatDate(date, "mmm dd, yyyy"),
              }}
            />
            <span>|</span>
            <span>{formatReadingTime(readingTime, "mins read")}</span>
          </div>
        </div>
      </div>
    </Link>
  )
}

export default PostCardV2
