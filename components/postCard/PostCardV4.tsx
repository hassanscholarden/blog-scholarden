import * as React from "react"

import Image from "next/image"
import Link from "next/link"

import MTAvatar from "@core/MaterialTailwind/Avatar"
import imagePlaceholder from "public/images/placeholder.png"
import type { PostCard } from "types/postCards"
import { formatDate } from "utils/date"

interface Props {
  data: PostCard
}

const PostCardV4: React.FC<Props> = ({ data }) => {
  const { title, slug, date, featuredMedia, author } = data

  return (
    <Link href={`/${slug}`}>
      <div className="grid cursor-pointer grid-cols-1 md:grid-cols-7">
        <div className="relative  col-span-1 mb-4 min-h-[247px] w-full overflow-hidden rounded-[15px] md:col-span-3 md:h-full">
          <Image
            className="object-cover"
            src={featuredMedia?.url ?? imagePlaceholder}
            alt={title}
            fill
          />
        </div>

        <div className="col-span-1 flex flex-1 flex-col justify-between px-4 md:col-span-4">
          <div className="space-y-8">
            <div className="flex items-center space-x-3">
              <MTAvatar
                src={author.avatarUrl}
                alt={author.name}
                size="sm"
                className="h-[32px] w-[32px]"
              />

              <h4 className="text-[17px] font-semibold text-gray-750">
                {author.name}
              </h4>
            </div>

            <p
              className="mb-4 border-b border-solid border-gray-400 py-4 text-[36px] font-medium"
              dangerouslySetInnerHTML={{ __html: title }}
            />
          </div>

          <h5
            dangerouslySetInnerHTML={{
              __html: formatDate(date, "html dd month, yy"),
            }}
            className="text-[17px] font-medium text-gray-750"
          />
        </div>
      </div>
    </Link>
  )
}

export default PostCardV4
