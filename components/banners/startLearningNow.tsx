import * as React from "react"

import { PRICING_PAGE_URL } from "constants/urls"

const StartLearningNowBanner: React.FC = () => {
  return (
    <div className="flex flex-col items-center justify-center bg-primary-600 px-4 py-6 text-white sm:flex-row sm:space-x-4 sm:bg-primary-500 sm:px-14 sm:py-8">
      <div className="w-full sm:w-auto sm:flex-1">
        {/* <h1 className="inline-block text-[17px] font-bold sm:hidden">
          Ready To Learn Now?
        </h1> */}
        {/* <h1 className="hidden text-[32px] font-bold sm:inline-block">
          Start Learning Now
        </h1> */}
        <h1 className="text-[17px] font-bold sm:text-[32px]">
          Start Learning Now
        </h1>
        <p className="mt-3 text-[13px] font-medium sm:text-[23px]">
          Improve 15+ points or get your money back!
        </p>
      </div>

      <a
        target="_blank"
        href={PRICING_PAGE_URL}
        className="mt-6 w-full rounded-[8px] bg-secondary-200 p-2 text-center text-[15px] font-medium text-primary-600 sm:mt-0 sm:w-auto sm:px-4 sm:py-2 sm:text-[17px]"
      >
        Buy Now
      </a>
    </div>
  )
}

export default StartLearningNowBanner
