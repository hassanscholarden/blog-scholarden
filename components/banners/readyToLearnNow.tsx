import * as React from "react"

import { PRICING_PAGE_URL } from "constants/urls"

const ReadyToLearnNowBanner: React.FC = () => {
  return (
    <section className="my-8 px-0 sm:my-14 sm:px-8 md:my-16 md:px-16 lg:px-20">
      <div className="flex flex-col items-center justify-center bg-gray-200 px-4 py-6 text-primary-600 sm:rounded-[10px] md:flex-row md:space-x-4 md:rounded-[16px] md:px-16 md:py-8">
        <div className="w-full md:w-auto md:flex-1">
          <h1 className="text-[17px] font-bold md:text-[32px]">
            Ready To Learn Now?
          </h1>

          <p className="mt-3 text-[13px] font-medium md:text-[23px]">
            Improve 15+ points or get your money back!
          </p>
        </div>

        <a
          target="_blank"
          href={PRICING_PAGE_URL}
          className="mt-6 w-full rounded-[8px] bg-secondary-200 p-2 text-center text-[15px] font-medium text-primary-600 md:mt-0 md:w-auto md:px-4 md:py-2 md:text-[17px]"
        >
          Buy Now
        </a>
      </div>
    </section>
  )
}

export default ReadyToLearnNowBanner
