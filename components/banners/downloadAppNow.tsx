import * as React from "react"

import Image from "next/image"

import {
  GRE_VOCABULARY_APP_GOOGLE_PLAY,
  GRE_VOCABULARY_APP_STORE,
} from "constants/urls"
import vocabAppScreens from "public/images/vocabulary-app-screens.png"
import appStoreLg from "public/svgs/app-store-lg.svg"
import playStoreLg from "public/svgs/google-play-lg.svg"

const DownloadAppNowBanner: React.FC = () => {
  const getAppButton: (
    icon: any,
    link: string,
    name: string
  ) => React.ReactElement = (icon, link, name) => (
    <a
      rel="noreferrer"
      target="_blank"
      href={link}
      className="relative flex max-w-[250px] items-center justify-center rounded-[12px]"
    >
      <Image src={icon} alt={name} priority className="object-contain" />
    </a>
  )

  return (
    <section className="mb-9 mt-4 md:my-14">
      <div className="mt-8 flex flex-col-reverse items-center justify-center space-x-0 rounded-[10px] bg-primary-100 px-4 py-6 sm:flex-row sm:space-x-14 md:mt-0 md:rounded-[16px] md:px-16 md:py-8">
        <div className="flex-1 space-y-5 md:space-y-10">
          <div>
            <p className="text-[17px] font-bold text-primary-600 sm:text-[20px] md:text-[23px]">
              Download App Now!
            </p>

            <p className="text-[14px] sm:text-[18px] md:text-[20.5px] md:font-medium">
              GRE High-Frequency Vocabulary App
            </p>
          </div>

          <div className="flex space-x-4">
            {getAppButton(
              playStoreLg,
              GRE_VOCABULARY_APP_GOOGLE_PLAY,
              "playstore"
            )}

            {getAppButton(appStoreLg, GRE_VOCABULARY_APP_STORE, "app store")}
          </div>
        </div>

        <div className="relative mb-8 h-[210px] w-[180px] md:mb-0 md:h-[260px] md:w-[200px]">
          <Image
            src={vocabAppScreens}
            alt="scholarden vocab app"
            className="object-contain"
            fill
            priority
          />
        </div>
      </div>
    </section>
  )
}

export default DownloadAppNowBanner
