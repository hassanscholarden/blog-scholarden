"use client"

import * as React from "react"

import { Menu, MenuHandler, MenuItem, MenuList } from "@material-tailwind/react"
import Link from "next/link"

import { CATEGORIES_PAGE } from "constants/routes"
import type { Category } from "types/category"
import classNames from "utils/classNames"

interface Props {
  subcategories?: Category[] | null
  title: string
  slug: string
  isSelected: boolean
  url: string
}

const CategoryCard: React.FC<Props> = (props) => {
  const { subcategories, title, slug, url, isSelected = false } = props

  const [openMenu, setOpenMenu] = React.useState(false)

  const classes = isSelected
    ? "bg-primary-600 text-white"
    : "border-primary-200 text-primary-600"

  const triggers = {
    onMouseEnter: () => {
      setOpenMenu(true)
    },
    onMouseLeave: () => {
      setOpenMenu(false)
    },
  }

  return Array.isArray(subcategories) ? (
    <Menu open={openMenu} handler={setOpenMenu}>
      <MenuHandler>
        <div
          id={Array.isArray(subcategories) ? subcategories[0].slug : slug}
          className="py-2"
        >
          <div
            {...triggers}
            className={classNames(
              "disable-user-select flex min-w-[64px] cursor-auto select-none items-center justify-center space-x-2 rounded-full border border-solid px-3 py-2 md:min-w-[80px] md:py-3",
              classes
            )}
          >
            <p
              className="w-auto flex-1 overflow-hidden whitespace-nowrap text-center text-[13px] font-medium sm:text-[15px] md:text-[17px] md:font-semibold"
              dangerouslySetInnerHTML={{ __html: title }}
            />

            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className={`h-3.5 w-3.5 transition-transform ${
                openMenu ? "rotate-180" : ""
              }`}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M19.5 8.25l-7.5 7.5-7.5-7.5"
              />
            </svg>
          </div>
        </div>
      </MenuHandler>

      <MenuList {...triggers}>
        {subcategories.map((subcategory) => (
          <Link
            href={`${CATEGORIES_PAGE}/${subcategory.slug}`}
            id={slug}
            key={subcategory.id}
          >
            <MenuItem>
              <span
                className="w-auto flex-1 overflow-hidden whitespace-nowrap text-center text-[13px] font-medium text-primary-600 sm:text-[15px] md:text-[17px] md:font-semibold"
                dangerouslySetInnerHTML={{ __html: subcategory.name }}
              />
            </MenuItem>
          </Link>
        ))}
      </MenuList>
    </Menu>
  ) : (
    <Link href={url.length > 0 && url !== "all" ? url : "/"} id={slug}>
      <div className="py-2">
        <div
          className={classNames(
            "disable-user-select flex min-w-[64px] cursor-pointer select-none items-center justify-center rounded-full border border-solid px-4 py-2 md:min-w-[80px] md:px-8 md:py-3",
            classes
          )}
        >
          <p
            className="w-auto overflow-hidden whitespace-nowrap text-center text-[13px] font-medium sm:text-[15px] md:text-[17px] md:font-semibold"
            dangerouslySetInnerHTML={{ __html: title }}
          />
        </div>
      </div>
    </Link>
  )
}

export default CategoryCard
