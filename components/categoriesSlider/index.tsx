/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
"use client"

import * as React from "react"

import { useRouter } from "next/navigation"

import CategoryCard from "./categoryCard"
import Icon from "@core/icon"
import { CATEGORIES_PAGE } from "constants/routes"
import type { Category } from "types/category"
import classNames from "utils/classNames"

interface Props {
  data: Category[]
  title: string
  activeCategorySlug?: "all" | string
}

const arrowButtonWidthClasses =
  "absolute inset-y-0 flex items-center w-full max-w-[55px] sm:max-w-[95px] md:max-w-[135px]"

const getCategoriesSlugsForSlider: (data: Category[]) => string[] = (data) => {
  return data.map(({ count, subcategories, slug }) => {
    if (count > 0) {
      return slug
    }

    if (Array.isArray(subcategories) && subcategories.length > 0) {
      return subcategories[0].slug
    }

    return ""
  })
}

const CategoriesSlider: React.FC<Props> = (props) => {
  const { data, title, activeCategorySlug = "all" } = props
  const categorySlugs = getCategoriesSlugsForSlider(data)

  const router = useRouter()

  const activeIndex = React.useMemo(() => {
    let index = 0

    if (activeCategorySlug !== "all") {
      const slugIndex = categorySlugs.findIndex(
        (slug) => slug === activeCategorySlug
      )

      if (slugIndex !== -1) {
        index = slugIndex + 1
      }
    }

    return index
  }, [activeCategorySlug, categorySlugs])

  function scrollHorizontallyToCard(cardId: string): void {
    const container = document.getElementById("category-slider-container")
    const card = document.getElementById(cardId)

    const documentWidth = document?.body?.clientWidth

    const offset = documentWidth >= 900 ? 125 : documentWidth >= 600 ? 85 : 25

    if (container != null && card != null) {
      const containerRect = container.getBoundingClientRect()
      const cardRect = card.getBoundingClientRect()
      const scrollLeft = cardRect.left - containerRect.left

      container.scrollTo({
        left: scrollLeft - offset,
        behavior: "smooth",
      })
    }
  }

  const handleCategoryCardClick: (dir: "prev" | "next") => any = (dir) => {
    let slug = "/"
    let index = activeIndex + (dir === "prev" ? -1 : 1)

    const slugs = ["all", ...categorySlugs]

    if (index < 0) {
      index = 0
    }
    if (index > slugs.length - 1) {
      index = slugs.length - 1
    }

    slug = slugs[index]

    if (slug === "all") {
      slug = "/"
    } else {
      slug = `${CATEGORIES_PAGE}/${slug}`
    }

    router.push(slug)
  }

  React.useEffect(() => {
    scrollHorizontallyToCard(activeCategorySlug)
  }, [activeCategorySlug])

  return (
    <div className="my-2 select-none space-y-2 py-[15px] md:my-4 md:space-y-4 md:py-[15px]">
      <p className="section-title text-[17px] md:text-[19px]">{title}</p>

      <div className="relative">
        {activeIndex !== 0 && (
          <span
            className={classNames(
              "inline-block md:hidden",
              arrowButtonWidthClasses,
              "left-0 justify-start"
            )}
            style={{
              background:
                "linear-gradient(90deg, #FFF 20.40%, rgba(255, 255, 255, 0.85) 40.32%, rgba(255, 255, 255, 0.71) 55.31%, rgba(255, 255, 255, 0.00) 94.45%)",
            }}
          >
            <button
              name="previous category"
              onClick={() => handleCategoryCardClick("prev")}
              className="p-4"
            >
              <Icon
                iconName="chevron-left"
                className="h-[17px] w-[17px] md:h-[23px] md:w-[23px]"
              />
            </button>
          </span>
        )}

        <div
          id="category-slider-container"
          className="no-scrollbar flex space-x-2 overflow-x-scroll md:space-x-4"
        >
          <CategoryCard
            title="All"
            slug=""
            url=""
            isSelected={activeCategorySlug === "all"}
          />

          {data
            .sort((a, b) => a.name.length - b.name.length)
            .map(({ id, name, slug, subcategories }, index) => (
              <CategoryCard
                key={id}
                title={name}
                slug={slug}
                url={`${CATEGORIES_PAGE}/${slug}`}
                isSelected={
                  activeCategorySlug ===
                  (Array.isArray(subcategories) ? subcategories[0].slug : slug)
                }
                subcategories={subcategories}
              />
            ))}
        </div>

        {activeIndex < data.length && (
          <span
            className={classNames(
              "inline-block md:hidden",
              arrowButtonWidthClasses,
              "right-0 justify-end"
            )}
            style={{
              background:
                "linear-gradient(270deg, #FFF 20.40%, rgba(255, 255, 255, 0.85) 40.32%, rgba(255, 255, 255, 0.71) 55.31%, rgba(255, 255, 255, 0.00) 94.45%)",
            }}
          >
            <button
              name="next category"
              onClick={() => handleCategoryCardClick("next")}
              className="p-4"
            >
              <Icon
                iconName="chevron-right"
                className="h-[17px] w-[17px] md:h-[23px] md:w-[23px]"
              />
            </button>
          </span>
        )}
      </div>
    </div>
  )
}

export default CategoriesSlider
