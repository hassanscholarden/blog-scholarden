"use client"

import React, { type FC } from "react"

import {
  Button,
  Dialog,
  DialogHeader,
  DialogBody,
  DialogFooter,
} from "@material-tailwind/react"

import type { handlerType } from "types/common"

interface Props {
  title?: string
  open: boolean
  children: React.ReactNode
  allowActions?: boolean
  dividers?: boolean
  onClose: () => any
}

const DialogModal: FC<Props> = (props) => {
  const {
    title,
    children,
    allowActions = true,
    dividers = true,
    open,
    onClose,
  } = props

  const handleClose: handlerType = () => {
    onClose()
  }

  return (
    <Dialog open={open} handler={handleClose}>
      {title != null && <DialogHeader>{title} </DialogHeader>}

      <DialogBody divider={dividers}>{children}</DialogBody>

      {allowActions && (
        <DialogFooter>
          <Button
            variant="text"
            color="red"
            onClick={handleClose}
            className="mr-1"
            name="Cancel"
          >
            <span>Cancel</span>
          </Button>
          <Button
            variant="gradient"
            color="purple"
            onClick={handleClose}
            name="Confirm"
          >
            <span>Confirm</span>
          </Button>
        </DialogFooter>
      )}
    </Dialog>
  )
}

export default DialogModal
