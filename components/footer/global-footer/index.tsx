import * as React from "react"

import Image from "next/image"
import Link from "next/link"

import { HELP_EMAIL_ADDRESS } from "constants/index"
import { footerAccountLinks, socialLinksData } from "constants/navItems"
import {
  FACEBOOK_MESSENGER,
  GRE_VOCABULARY_APP_GOOGLE_PLAY,
  GRE_VOCABULARY_APP_STORE,
} from "constants/urls"
import MessengerIcon from "public/icons/social/messenger-blue.svg"
import Logo from "public/logo.png"
import AppStoreIcon from "public/svgs/app-store-sm.svg"
import GooglePlayIcon from "public/svgs/google-play-sm.svg"

const GlobalFooter: React.FC = () => {
  return (
    <div className="grid grid-cols-1 gap-8 px-4 py-6 sm:px-14 sm:py-8 md:grid-cols-4">
      {/* Column 1 */}
      <div className="grid grid-cols-1 gap-5 md:col-span-2">
        <Link href="/">
          <div className="relative h-[25px] w-[130px] cursor-pointer md:h-[30px] md:w-[145px]">
            <Image
              src={Logo}
              alt="scholarden"
              priority
              fill
              className="object-contain"
            />
          </div>
        </Link>

        {/* Vocab app promotion */}
        <div className="grid grid-cols-1 gap-6 md:gap-8">
          <div className="text-[12px] md:text-[20px]">
            <span className="mb-1 font-semibold">Download</span>
            <p>GRE High-Frequency Vocabulary App</p>
          </div>

          <div className="flex items-center justify-between sm:justify-start sm:space-x-10">
            <a
              rel="noreferrer"
              target="_blank"
              href={GRE_VOCABULARY_APP_GOOGLE_PLAY}
              className="relative h-[38px] w-[140px] cursor-pointer md:h-[56px] md:w-[193px]"
            >
              <Image src={GooglePlayIcon} alt="playstore" fill loading="lazy" />
            </a>
            <a
              target="_blank"
              rel="noreferrer"
              href={GRE_VOCABULARY_APP_STORE}
              className="relative h-[38px] w-[140px] cursor-pointer md:h-[56px] md:w-[193px]"
            >
              <Image src={AppStoreIcon} alt="app store" fill loading="lazy" />
            </a>
          </div>
        </div>
      </div>

      {/* Column 2: Accounts links */}
      <div className="space-y-0 md:space-y-4">
        <span className="footer-col-title">Accounts</span>

        <ul className="flex justify-between space-x-2 space-y-0 sm:justify-start md:flex-col md:space-x-0 md:space-y-3">
          {footerAccountLinks.map((link, index) => {
            return (
              <li
                key={index}
                className="cursor-pointer text-[13px] font-medium text-gray-750"
              >
                {link.type === "external" ? (
                  <a href={link.url} target="_blank">
                    {link.title}
                  </a>
                ) : (
                  <Link href={link.url}>{link.title}</Link>
                )}
              </li>
            )
          })}
        </ul>
      </div>

      {/* Column 3 */}

      <div className="space-y-8">
        <div className="space-y-4">
          <span className="footer-col-title">Follow ScholarDen</span>

          <ul className="flex flex-wrap justify-between space-x-2 sm:justify-start">
            {socialLinksData.map((icon) => (
              <li key={icon.id} className="my-1">
                <Link
                  href={icon.link}
                  target={icon.linkType === "external" ? "_blank" : ""}
                  className="cursor-pointer"
                >
                  <div className="relative h-[38px] w-[38px]">
                    <Image src={icon.icon} alt={icon.alt} fill />
                  </div>
                </Link>
              </li>
            ))}
          </ul>
        </div>

        <div className="space-y-4">
          <span className="footer-col-title">Contact Us</span>

          <div className="flex items-center text-[13px] md:text-[15px]">
            <a href={FACEBOOK_MESSENGER} target="_blank" rel="noreferrer">
              <div className="relative h-[38px] w-[38px]">
                <Image src={MessengerIcon} alt="messenger" fill priority />
              </div>
            </a>
            &nbsp; &nbsp;
            <span>or</span> &nbsp;&nbsp;
            <span>
              Email us at{" "}
              <a
                href={`mailto:${HELP_EMAIL_ADDRESS}`}
                className="font-medium text-primary-600"
              >
                {HELP_EMAIL_ADDRESS}
              </a>
            </span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default GlobalFooter
