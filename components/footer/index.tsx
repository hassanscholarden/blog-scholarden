import * as React from "react"

import GlobalFooter from "./global-footer"
import { TERMS_AND_CONDITION_PAGE_URL } from "constants/urls"

interface Props {
  children?: React.ReactNode
}
const Footer: React.FC<Props> = (props) => {
  const { children } = props

  return (
    <div className="bg-gray-100">
      {children !== undefined ? children : <GlobalFooter />}

      <div className="mt-4 border border-t-2 border-solid border-gray-400 py-4 text-center text-gray-750">
        Scholar Den, Inc © 2023 - All rights reserved. | &nbsp;
        <a href={TERMS_AND_CONDITION_PAGE_URL} target="_blank">
          Terms and Conditions
        </a>
      </div>
    </div>
  )
}

export default Footer
