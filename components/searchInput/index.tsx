"use client"

import { useState, useEffect, type FC, useRef, type FormEvent } from "react"

import { useAnimate, stagger, type AnimationScope } from "framer-motion"
import Link from "next/link"

import SearchListItem from "./SearchListItem"
import Button from "@core/button"
import Icon from "@core/icon"
import { fetchData } from "handlers"
import useClickOutside from "hooks/useClickOutside"
import { type SearchPostType } from "types/search"
import classNames from "utils/classNames"
import { extractSlug } from "utils/strings"

const staggerMenuItems = stagger(0.1, { startDelay: 0.15 })

interface SearchInputProps {
  placeholder: string
  searchURL: string
}

const useMenuAnimation: (isOpen: boolean) => AnimationScope<any> = (isOpen) => {
  const [scope, animate] = useAnimate()

  useEffect(() => {
    void animate(
      "ul",
      {
        clipPath: isOpen
          ? "inset(0% 0% 0% 0% round 0px)"
          : "inset(10% 50% 90% 50% round 0px)",
      },
      {
        type: "spring",
        bounce: 0,
        duration: 0.5,
      }
    )

    void animate(
      "li",
      isOpen
        ? { opacity: 1, scale: 1, filter: "blur(0px)" }
        : { opacity: 0, scale: 0, filter: "blur(20px)" },
      {
        duration: 0.08,
        delay: isOpen ? staggerMenuItems : 0,
      }
    )

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isOpen])

  return scope
}

const SearchInput: FC<SearchInputProps> = (props) => {
  const { placeholder, searchURL } = props

  const [loading, setLoading] = useState(false)
  const [isOpen, setIsOpen] = useState(false)
  const [searchQuery, setSearchQuery] = useState("")
  const scrollListRef = useRef<HTMLUListElement | null>(null)
  const scope = useMenuAnimation(isOpen)
  const elementRef = useRef(null)
  const [posts, setPosts] = useState<SearchPostType[]>([])

  const closeHandler: () => void = () => {
    setIsOpen(false)
    if (scrollListRef.current != null) {
      scrollListRef.current.scrollTo(0, 0)
    }
  }

  useClickOutside(elementRef, closeHandler)

  const searchHandler: any = async () => {
    if (loading) return

    if (searchQuery.length < 1) {
      setPosts([])
      closeHandler()
      return
    }
    try {
      setLoading(true)
      const resp = (await fetchData(
        searchURL + searchQuery + `&per_page=50`
      )) as SearchPostType[]

      setPosts(resp)
    } catch (error) {
    } finally {
      setLoading(false)
    }
  }

  const onsubmitHandler: (event: FormEvent<HTMLFormElement>) => void = (
    event
  ) => {
    event.preventDefault()

    searchHandler()
  }

  return (
    <div ref={elementRef}>
      <div ref={scope} className="relative w-full text-gray-900">
        <div
          className={classNames(
            "flex items-center justify-between pl-4 text-[15px] shadow-[rgba(50,_50,_105,_0.15)_0px_2px_5px_0px,_rgba(0,_0,_0,_0.05)_0px_1px_1px_0px] transition-all duration-500 md:pl-10 md:text-[17px]",
            isOpen ? "rounded-lg" : "rounded-full"
          )}
        >
          <form
            onSubmit={onsubmitHandler}
            className="flex flex-1 py-4 font-medium"
          >
            <input
              value={searchQuery}
              placeholder={placeholder}
              onChange={(e) => {
                setSearchQuery(e.target.value)
              }}
              onFocus={() => {
                setIsOpen(true)
              }}
              className="search-input flex-1"
            />
          </form>

          <Button
            variant="text"
            className="rounded-full"
            onClick={searchHandler}
            disabled
            name="search"
          >
            <Icon iconName="search" className="h-[30px] w-[30px]" />
          </Button>
        </div>

        <div
          className="absolute -bottom-96 left-0 z-10 h-96 w-full  bg-white px-10 py-4 shadow-[rgba(50,_50,_105,_0.15)_0px_2px_5px_0px,_rgba(0,_0,_0,_0.05)_0px_1px_1px_0px]"
          style={{ display: isOpen ? "inline-block" : "none" }}
        >
          <ul
            ref={scrollListRef}
            className="h-full overflow-y-scroll"
            style={{
              pointerEvents: isOpen ? "auto" : "none",
              clipPath: "inset(10% 50% 90% 50% round 0px)",
            }}
          >
            {loading ? (
              <li className="w-full text-center">Loading...</li>
            ) : posts.length < 1 ? (
              <li className="w-full text-center">Nothing to show </li>
            ) : (
              posts.map((post, index) => {
                const slug = extractSlug(post.url) ?? ""

                return (
                  <Link href={`/${slug}`} key={index}>
                    <SearchListItem>
                      <span dangerouslySetInnerHTML={{ __html: post.title }} />
                    </SearchListItem>
                  </Link>
                )
              })
            )}
          </ul>
        </div>
      </div>
    </div>
  )
}

export default SearchInput
