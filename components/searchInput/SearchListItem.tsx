"use client"

import { memo, type FC, type ReactNode } from "react"

interface ListItemProps {
  children: ReactNode
}
const SearchListItem: FC<ListItemProps> = ({ children }) => {
  return (
    <li className="cursor-pointer px-2 py-4 text-[17px] font-medium text-gray-900 hover:bg-gray-200 hover:text-primary-600">
      {children}
    </li>
  )
}

export default memo(SearchListItem)
