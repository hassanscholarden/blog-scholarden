/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */

"use client"

import { type FC, useEffect, useState } from "react"

import Icon from "@core/icon"
import classNames from "utils/classNames"

interface Props {
  fixed?: boolean
  hideText?: boolean
}
const ScrollToTop: FC<Props> = ({ fixed = true, hideText = true }) => {
  const [isVisible, setIsVisible] = useState(false)

  const toggleVisibility: () => void = () => {
    setIsVisible(window.pageYOffset > 300)
  }

  const scrollToTop: () => void = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    })
  }

  useEffect(() => {
    window.addEventListener("scroll", toggleVisibility)

    return () => {
      window.removeEventListener("scroll", toggleVisibility)
    }
  }, [])

  return (
    <div
      className={classNames(
        isVisible ? "opacity-100" : "opacity-0",
        fixed ? "fixed bottom-4 right-4 md:bottom-6 md:right-6" : "",
        "flex cursor-pointer select-none flex-col items-center justify-center space-y-2"
      )}
      onClick={scrollToTop}
    >
      <button
        name="scroll to top"
        className={classNames(
          "inline-flex items-center rounded-full border border-solid border-gray-800 bg-white p-3 shadow-sm transition-opacity hover:bg-gray-200 focus:outline-none md:p-4"
        )}
      >
        <Icon iconName="chevron-up" className="h-[15px] w-[15px]" />
      </button>
      {!hideText && (
        <p className="hidden text-gray-700 md:inline-block">back to top</p>
      )}
    </div>
  )
}

export default ScrollToTop
