import * as React from "react"

import CollapseAlert from "@core/collapseAlert"
import { postCommentHandler } from "handlers/postComments"
import type { postCommentDefaultAlertMessageType } from "types/comments"
import type { mouseEventHandlerType, formEventType } from "types/common"
import classNames from "utils/classNames"
import { capitalizedAllWordsInText } from "utils/strings"

interface Props {
  postId: number
  commentId?: number
}

const defaultAlertMessage: postCommentDefaultAlertMessageType = {
  show: false,
  message: "",
  type: "success",
}

const inputClasses =
  "rounded-md border-2 border-solid border-gray-400 p-2 text-gray-800 text-[17px]"

const PostCommentForm: React.FC<Props> = (props) => {
  const { postId, commentId } = props

  const [content, setContent] = React.useState("")
  const [name, setName] = React.useState("")
  const [email, setEmail] = React.useState("")

  const [submitting, setSubmitting] = React.useState(false)
  const [alert, setAlert] = React.useState(defaultAlertMessage)

  const createComment: formEventType = async (event) => {
    event.preventDefault()

    const commentData: any = {
      post: postId,
      author_name: name,
      author_email: email,
      content,
      parent: commentId ?? 0,
    }

    if (commentId == null) {
      delete commentData.parent
    }

    setSubmitting(true)

    const { message, type, comment } = await postCommentHandler(commentData)

    setAlert({
      show: true,
      message,
      type,
    })

    if (type === "success" && comment !== null) {
      setContent("")
      setName("")
      setEmail("")
    }

    setSubmitting(false)
  }
  const onAlertClose: mouseEventHandlerType = (event) => {
    event.preventDefault()
    setAlert((prev) => ({
      ...defaultAlertMessage,
      type: prev.type,
    }))
  }

  return (
    <form
      onSubmit={(event) => {
        void createComment(event)
      }}
      className="grid grid-cols-2 gap-3 text-gray-900 md:gap-6"
    >
      <p className="col-span-2 text-[23px] ">Comment</p>

      <input
        value={name}
        onChange={(e) => {
          setName(capitalizedAllWordsInText(e.target.value))
        }}
        disabled={submitting}
        required
        placeholder="*Your name"
        className={classNames(inputClasses, "comment-input col-span-1")}
      />
      <input
        value={email}
        onChange={(e) => {
          setEmail(e.target.value.replaceAll(" ", ""))
        }}
        type="email"
        required
        disabled={submitting}
        placeholder="*Your email ID"
        className={classNames(inputClasses, "comment-input col-span-1")}
      />
      <input
        value={content}
        required
        onChange={(e) => {
          setContent(e.target.value)
        }}
        disabled={submitting}
        placeholder="Write here..."
        className={classNames(inputClasses, "comment-input col-span-2")}
      />

      {alert.show && (
        <div className="col-span-2">
          <CollapseAlert
            open={alert.show}
            message={alert.message}
            type={alert.type}
            onClose={onAlertClose}
          />
        </div>
      )}

      <div>
        <input
          // disabled={disableButton}
          type="submit"
          value={submitting ? "Submitting..." : "Submit Response"}
          className={classNames(
            "w-full max-w-[300px] cursor-pointer rounded-md bg-primary-600 px-5 py-2 text-center text-[17px] font-medium text-white"
          )}
        />
      </div>
    </form>
  )
}

export default PostCommentForm
