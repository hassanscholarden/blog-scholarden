"use client"

import * as React from "react"

import PostCommentForm from "./postCommentForm"
import DialogModal from "components/modal"
import useModal from "hooks/useModal"
import { getTimeDifference } from "utils/date"

interface Props {
  postId: number
  commentId: number
  date: number
}
type handleReplyType = () => void

const ActionsButtonRow: React.FC<Props> = (props) => {
  const { postId, commentId, date } = props

  const [showReplyModal, toggleShowReplyModal] = useModal(false)

  const handleReply: handleReplyType = () => {
    toggleShowReplyModal()
  }

  return (
    <div>
      <div className="flex flex-row ">
        <button
          onClick={handleReply}
          className="comment-action-button"
          name="reply comment"
        >
          Reply
        </button>

        <span className="text-primary-600">・</span>

        <span className="text-gray-700">{getTimeDifference(date)}</span>
      </div>

      <DialogModal
        open={showReplyModal}
        onClose={toggleShowReplyModal}
        allowActions={false}
        dividers={false}
      >
        <div>
          <PostCommentForm postId={postId} commentId={commentId} />
        </div>
      </DialogModal>
    </div>
  )
}

export default ActionsButtonRow
