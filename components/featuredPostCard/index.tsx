/* eslint-disable @typescript-eslint/restrict-template-expressions */
import * as React from "react"

import Link from "next/link"

import MTAvatar from "@core/MaterialTailwind/Avatar"
import { type PostCard } from "types/postCards"
import { formatDate } from "utils/date"

interface Props {
  post: PostCard | null
}

const FeaturedPostCard: React.FC<Props> = ({ post }) => {
  if (post === null) {
    return null
  }

  const { featuredMedia, date, title, categories, excerpt, author, slug } = post

  return (
    <Link href={`/${slug}`}>
      <h4 className="mt-4 inline-block text-[17px]	font-bold md:hidden">
        Featured |{" "}
        <span dangerouslySetInnerHTML={{ __html: categories[0].name }} />
      </h4>

      <div
        className="disable-user-select my-5 h-[175px] min-h-[175px] select-none overflow-hidden rounded-[15px] bg-cover bg-center bg-no-repeat sm:mb-14 sm:rounded-[28px] md:mx-0 md:my-20 md:h-[440px] md:min-h-[440px] md:rounded-[40px]"
        style={{
          backgroundImage: `url(${
            featuredMedia?.url ?? "public/images/placeholder.png"
          })`,
        }}
      >
        <div className="flex h-full w-full items-end bg-gradient-to-t from-gray-900 to-transparent p-[15px] text-white sm:p-[28px] md:p-[40px]">
          <div className="flex flex-col space-y-2 md:space-y-4">
            <h4 className="hidden text-[22px] md:inline-block">
              Featured |{" "}
              <span dangerouslySetInnerHTML={{ __html: categories[0].name }} />
            </h4>

            <h2
              className="text-[17px] font-bold md:max-w-[900px] md:text-5xl"
              dangerouslySetInnerHTML={{ __html: title }}
            />

            {excerpt.length > 0 && (
              <span className="hidden text-[15px] md:inline-block">
                <p
                  className="max-two-lines md:max-w-[750px]"
                  dangerouslySetInnerHTML={{ __html: excerpt }}
                />
              </span>
            )}

            <div className="flex items-center space-x-2 md:space-x-3">
              <MTAvatar
                src={author.avatarUrl}
                alt={author.name}
                className="h-[26px] w-[26px] md:h-[30px] md:w-[30px]"
              />

              <div className="flex items-center space-x-2 text-[12px] md:text-[15px]">
                <span className="font-semibold">{author.name}</span>
                <span>|</span>
                <span
                  dangerouslySetInnerHTML={{
                    __html: formatDate(date, "html dd month, yy"),
                  }}
                  className="font-medium"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Link>
  )
}

export default FeaturedPostCard
